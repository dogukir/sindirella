package com.company.sindirella.activitys

import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import com.company.sindirella.R
import com.company.sindirella.controller.LoginController
import com.google.gson.JsonObject
import top.defaults.drawabletoolbox.DrawableBuilder

class LoginActivity : AppCompatActivity(), LoginController.onPostRegisterListener {

    private var loginButton : Button? = null
    private var mailEdit : EditText? = null
    private var passwordEdit : EditText? = null
    private var dateStartText : TextView? = null
    private var continueButton : Button? = null
    private var googleConnectButton : Button? = null
    private var loginController : LoginController? = null
    var registerBody: JsonObject? = null


    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        supportActionBar?.hide()

        loginButton = findViewById(R.id.loginButton)
        mailEdit = findViewById(R.id.mailEdit)
        passwordEdit = findViewById(R.id.passwordEdit)
        dateStartText = findViewById(R.id.dateStartText)
        continueButton = findViewById(R.id.continueButton)
        googleConnectButton = findViewById(R.id.googleConnectButton)

        loginController = LoginController(this,null)
        loginController?.postRegisterListener = this

        registerBody = JsonObject()

        registerBody?.addProperty("email","")
        registerBody?.addProperty("password","123A")
        registerBody?.addProperty("retypedPassword","12A")
        registerBody?.addProperty("name","doğukan")


        loginController?.postRegister(registerBody)



        setUI()

    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun setUI(){

        window.statusBarColor = resources.getColor(R.color.how_working_status_bar)
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR

        loginButton?.background = DrawableBuilder().solidColor(resources.getColor(R.color.orange)).cornerRadius(50).build()
        continueButton?.background = DrawableBuilder().solidColor(resources.getColor(R.color.black)).cornerRadius(50).build()
        googleConnectButton?.background = DrawableBuilder().solidColor(resources.getColor(R.color.transparent)).strokeColor(resources.getColor(R.color.black)).strokeWidth(3).cornerRadius(50).build()


    }

    override fun onPostRegisterListener(responseOk: Boolean, register: RegisterResponse?, failMessage: Int, error: String) {

        if (responseOk){

            if (register != null){

                println("dogru")
                register

            }else{

                println("data boş")
            }


        }else{

            println("yanlış döndü")

        }

    }

}