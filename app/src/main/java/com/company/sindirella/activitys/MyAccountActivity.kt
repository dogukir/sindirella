package com.company.sindirella.activitys

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import com.company.sindirella.R
import com.company.sindirella.view.MenuItemView
import java.util.ArrayList

class MyAccountActivity : AppCompatActivity() {

    private var addView : LinearLayout? = null
    private var backPressedText : TextView? = null


    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_account)
        supportActionBar?.hide()

        addView = findViewById(R.id.addView)
        backPressedText = findViewById(R.id.backPressedText)

        addView?.removeAllViews()


        var menuArray = ArrayList<String>()

        menuArray.add("Siparişlerim")
        menuArray.add("Sipariş Geçmişim")
        menuArray.add("30₺ Kazan")
        menuArray.add("Profilim")
        menuArray.add("Ayarlar")

        for (i in 0..menuArray.size-1){

            val menuView = MenuItemView(this)

            menuView.setTitle(menuArray.get(i))

            addView?.addView(menuView)

        }



        backPressedText?.setOnClickListener {

            onBackPressed()

        }


        setUI()

    }


    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun setUI(){

        window.statusBarColor = resources.getColor(R.color.how_working_status_bar)
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR

    }
}