package com.company.sindirella.activitys

import android.app.DatePickerDialog
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.Spinner
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager.widget.ViewPager
import com.company.sindirella.R
import com.company.sindirella.helpers.ProductDetailViewPagerAdapter
import top.defaults.drawabletoolbox.DrawableBuilder
import java.text.SimpleDateFormat
import java.util.*

class ProductDetailActivity : AppCompatActivity() {

    private var viewPager : ViewPager? = null
    private var spinnerBody : Spinner? = null
    private var startDateText : TextView ? = null
    private var endDateText : TextView? = null
    private var commentButton : Button? = null
    private var returnConditionsButton : Button? = null
    private var confidentialityAgreementButton : Button? = null
    private var addBasketButton : Button? = null
    private var adapter : ProductDetailViewPagerAdapter? = null
    private var startDateView : View? = null
    private var endDateView : View? = null
    private var spinnerView : View? = null



    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_detail)
        supportActionBar?.hide()

        viewPager = findViewById(R.id.viewPager)
        spinnerBody = findViewById(R.id.spinnerBody)
        startDateText = findViewById(R.id.startDateText)
        endDateText = findViewById(R.id.endDateText)
        commentButton = findViewById(R.id.commentButton)
        returnConditionsButton = findViewById(R.id.returnConditionsButton)
        confidentialityAgreementButton = findViewById(R.id.confidentialityAgreementButton)
        addBasketButton = findViewById(R.id.addBasketButton)
        startDateView = findViewById(R.id.startDateView)
        endDateView = findViewById(R.id.endDateView)
        spinnerView = findViewById(R.id.spinnerView)



        var arr = ArrayList<String>()

        arr.add("dogu")
        arr.add("dogu")
        arr.add("dogu")
        arr.add("dogu")
        arr.add("dogu")
        arr.add("dogu")

        adapter = ProductDetailViewPagerAdapter(this,arr)
        viewPager?.adapter = adapter








        var controllVal = 0

        val sdf = SimpleDateFormat("dd/MM/yyyy", Locale.ROOT)
        var calendarStart = Calendar.getInstance()
        var calendarEnd = Calendar.getInstance()

        val date = DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
            if (controllVal == 1) {

                calendarStart.set(Calendar.YEAR, year)
                calendarStart.set(Calendar.MONTH, monthOfYear)
                calendarStart.set(Calendar.DAY_OF_MONTH, dayOfMonth)

                startDateText?.text = sdf.format(calendarStart.time)
                startDateText?.setTextColor(resources.getColor(R.color.black))

            } else {

                calendarEnd.set(Calendar.YEAR, year)
                calendarEnd.set(Calendar.MONTH, monthOfYear)
                calendarEnd.set(Calendar.DAY_OF_MONTH, dayOfMonth)

                endDateText?.text = sdf.format(calendarEnd.time)
                endDateText?.setTextColor(resources.getColor(R.color.black))

            }
        }

        startDateText?.setOnClickListener {
            controllVal = 1
            DatePickerDialog(this, R.style.DatePickerTheme, date, calendarStart.get(Calendar.YEAR), calendarStart.get(
                Calendar.MONTH), calendarStart.get(Calendar.DAY_OF_MONTH)).show()
        }

        endDateText?.setOnClickListener {
            controllVal = 2
            DatePickerDialog(this, R.style.DatePickerTheme, date, calendarEnd.get(Calendar.YEAR), calendarEnd.get(
                Calendar.MONTH), calendarEnd.get(Calendar.DAY_OF_MONTH)).show()
        }




        setUI()

    }


    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun setUI(){

        window.statusBarColor = resources.getColor(R.color.how_working_status_bar)
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR

        commentButton?.background = DrawableBuilder().solidColor(resources.getColor(R.color.black)).cornerRadius(50).build()
        returnConditionsButton?.background = DrawableBuilder().solidColor(resources.getColor(R.color.transparent)).strokeColor(resources.getColor(R.color.black)).strokeWidth(3).cornerRadius(50).build()
        confidentialityAgreementButton?.background = DrawableBuilder().solidColor(resources.getColor(R.color.transparent)).strokeColor(resources.getColor(R.color.black)).strokeWidth(3).cornerRadius(50).build()
        addBasketButton?.background = DrawableBuilder().solidColor(resources.getColor(R.color.orange)).cornerRadius(50).build()

        startDateView?.background = DrawableBuilder().solidColor(resources.getColor(R.color.view_background)).cornerRadius(50).build()
        endDateView?.background = DrawableBuilder().solidColor(resources.getColor(R.color.view_background)).cornerRadius(50).build()
        spinnerView?.background = DrawableBuilder().solidColor(resources.getColor(R.color.view_background)).cornerRadius(50).build()

    }

}