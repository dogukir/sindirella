package com.company.sindirella.activitys

import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.company.sindirella.R
import com.company.sindirella.helpers.CommentActivityRecyclerAdapter
import top.defaults.drawabletoolbox.DrawableBuilder
import java.util.ArrayList

class CommentsActivity : AppCompatActivity() {

    private var recyclerView : RecyclerView? = null
    private var buttonComment : Button? = null
    private var adapter : CommentActivityRecyclerAdapter? = null
    private var backPressedText : TextView? = null



    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_comments)
        supportActionBar?.hide()

        recyclerView = findViewById(R.id.recyclerView)
        buttonComment = findViewById(R.id.buttonComment)
        backPressedText = findViewById(R.id.backPressedText)

        var arr = ArrayList<String>()

        arr.add("doğukan")
        arr.add("batıkan")
        arr.add("kuzeykan")
        arr.add("ağlakan")

        adapter = CommentActivityRecyclerAdapter(this,arr)
        recyclerView?.adapter = adapter

        val Layout = LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false)

        recyclerView?.layoutManager = Layout





        backPressedText?.setOnClickListener {

            onBackPressed()

        }


        setUI()

    }


    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun setUI(){

        window.statusBarColor = resources.getColor(R.color.how_working_status_bar)
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        buttonComment?.background = DrawableBuilder().solidColor(resources.getColor(R.color.splah_status_bar)).cornerRadius(50).build()

    }

}