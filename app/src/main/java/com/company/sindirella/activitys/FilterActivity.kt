package com.company.sindirella.activitys

import android.app.ActionBar
import android.app.DatePickerDialog
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import android.widget.GridView
import android.widget.Spinner
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import com.company.sindirella.R
import com.company.sindirella.helpers.FilterActivityGridViewAdapter
import java.text.SimpleDateFormat
import java.util.*

class FilterActivity : AppCompatActivity() {

    private var bedenGrid : GridView? = null
    private var spinner : Spinner? = null
    private var startDateText : TextView? = null
    private var endDateText : TextView? = null
    private var adapterGrid : FilterActivityGridViewAdapter? = null
    private var backPressedText : TextView? = null


    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_filter)
        supportActionBar?.hide()

        bedenGrid = findViewById(R.id.bedenGrid)
        spinner = findViewById(R.id.spinner)
        startDateText = findViewById(R.id.startDateText)
        endDateText = findViewById(R.id.endDateText)
        backPressedText = findViewById(R.id.backPressedText)




        var arr = resources.getStringArray(R.array.spinner_default_value)

        var adapter = ArrayAdapter<String>(this, R.layout.spinner_item, R.id.spinnerTextView, arr)

        spinner?.adapter = adapter



        bedenGrid?.numColumns = 6
        bedenGrid?.horizontalSpacing = 10
        bedenGrid?.verticalSpacing = 10

        var arrSize = ArrayList<String>()

        arrSize.add("2")
        arrSize.add("4")
        arrSize.add("6")
        arrSize.add("10")
        arrSize.add("12")
        arrSize.add("14")
        arrSize.add("16")
        arrSize.add("30")
        arrSize.add("32")
        arrSize.add("34")
        arrSize.add("36")
        arrSize.add("38")
        arrSize.add("S")
        arrSize.add("M")
        arrSize.add("L")
        arrSize.add("XL")
        arrSize.add("XLL")

        var params = bedenGrid?.layoutParams

        if (arrSize.size > 12){
            params?.height = 300
        }

        bedenGrid?.layoutParams = params

        adapterGrid = FilterActivityGridViewAdapter(this,arrSize)

        bedenGrid?.adapter = adapterGrid





        var controllVal = 0

        val sdf = SimpleDateFormat("dd/MM/yyyy", Locale.ROOT)
        var calendarStart = Calendar.getInstance()
        var calendarEnd = Calendar.getInstance()

        val date = DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
            if (controllVal == 1) {

                calendarStart.set(Calendar.YEAR, year)
                calendarStart.set(Calendar.MONTH, monthOfYear)
                calendarStart.set(Calendar.DAY_OF_MONTH, dayOfMonth)

                startDateText?.text = sdf.format(calendarStart.time)
                startDateText?.setTextColor(resources.getColor(R.color.black))

            } else {

                calendarEnd.set(Calendar.YEAR, year)
                calendarEnd.set(Calendar.MONTH, monthOfYear)
                calendarEnd.set(Calendar.DAY_OF_MONTH, dayOfMonth)

                endDateText?.text = sdf.format(calendarEnd.time)

            }
        }

        startDateText?.setOnClickListener {
            controllVal = 1
            DatePickerDialog(this, R.style.DatePickerTheme, date, calendarStart.get(Calendar.YEAR), calendarStart.get(
                Calendar.MONTH), calendarStart.get(Calendar.DAY_OF_MONTH)).show()
        }

        startDateText?.setOnClickListener {
            controllVal = 2
            DatePickerDialog(this, R.style.DatePickerTheme, date, calendarEnd.get(Calendar.YEAR), calendarEnd.get(
                Calendar.MONTH), calendarEnd.get(Calendar.DAY_OF_MONTH)).show()
        }


        backPressedText?.setOnClickListener {

            onBackPressed()

        }



        setUI()


    }


    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun setUI(){

        window.statusBarColor = resources.getColor(R.color.how_working_status_bar)
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR

    }

}