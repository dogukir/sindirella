package com.company.sindirella.activitys

import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.company.sindirella.R
import com.company.sindirella.helpers.BasketRecycleAdapter
import java.util.ArrayList
import top.defaults.drawabletoolbox.DrawableBuilder

class BasketActivity : AppCompatActivity() {

    private var recycleBasket : RecyclerView? = null
    private var adapter : BasketRecycleAdapter? = null
    private var complateShoppingButton : Button? = null
    private var backPressedImage : ImageView? = null

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_basket)
        supportActionBar?.hide()


        var arr = ArrayList<String>()

        arr.add("dogukan")
        arr.add("dogu")
        arr.add("do")
        arr.add("dogukan")
        arr.add("doguka")
        arr.add("doguk")


        recycleBasket = findViewById(R.id.recycleBasket)
        complateShoppingButton = findViewById(R.id.complateShoppingButton)
        backPressedImage = findViewById(R.id.backPressedImage)

        adapter = BasketRecycleAdapter(this,arr)

        recycleBasket?.adapter = adapter

        val layoutManager = LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false)

        recycleBasket?.layoutManager = layoutManager
        recycleBasket?.itemAnimator = DefaultItemAnimator()





        backPressedImage?.setOnClickListener {

            onBackPressed()

        }


        setUI()

    }


    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun setUI(){

        window.statusBarColor = resources.getColor(R.color.how_working_status_bar)
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR

        complateShoppingButton?.background = DrawableBuilder().solidColor(resources.getColor(R.color.orange)).cornerRadius(50).build()


    }


}