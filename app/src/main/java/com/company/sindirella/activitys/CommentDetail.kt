package com.company.sindirella.activitys

import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager.widget.ViewPager
import com.company.sindirella.R
import com.company.sindirella.helpers.CommentDetailViewPagerAdapter
import java.util.ArrayList

class CommentDetail :AppCompatActivity() {

    private var viewPager : ViewPager? = null
    private var adapter : CommentDetailViewPagerAdapter? = null
    private var backPressedText : TextView? = null


    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_comment_detail)
        supportActionBar?.hide()

        viewPager = findViewById(R.id.viewPager)
        backPressedText = findViewById(R.id.backPressedText)


        var arr = ArrayList<String>()

        arr.add("dogu")
        arr.add("dogu")
        arr.add("dogu")
        arr.add("dogu")
        arr.add("dogu")




        adapter = CommentDetailViewPagerAdapter(this,arr)

        viewPager?.adapter = adapter




        backPressedText?.setOnClickListener {

            onBackPressed()

        }


        setUI()

    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun setUI(){

        window.statusBarColor = resources.getColor(R.color.how_working_status_bar)
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR

    }


}