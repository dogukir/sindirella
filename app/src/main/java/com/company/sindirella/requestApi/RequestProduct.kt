package com.company.sindirella.requestApi

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.*
import com.company.sindirella.response.Payment.GetPaymentIdResponse
import com.company.sindirella.response.Product.ProductResponse
import retrofit2.http.Query
import java.util.ArrayList

class RequestProduct(activity: AppCompatActivity?, fragment: Fragment?, id: Int?,idArray: ArrayList<Int>?, brand: String?,
                     brandArray: ArrayList<String>?,color: String?,colorArray: ArrayList<String>?,
                     activities: String?,activitiesArray: ArrayList<String>?,
                     inventoriesSize: String?,inventoriesSizeArray: ArrayList<String>?,
                     categories: String?,categoriesArray: ArrayList<String>?,
                     description: String?, name: String?, priceRentalBetween: String?,
                     priceRentalGt: String?, priceRentalGte: String?,
                     priceRentalLt: String?, priceRentalLte: String?,
                     exist: Boolean?, orderPriceRental: String?,
                     page: Int?, listener: NetworkResponseListener<ArrayList<ProductResponse>>) {

    init {
        val request = RequestCreator.create<Service.Product>(Service.Product::class.java, NetworkSupport.NetworkAdress.base_url)
        request.getProduct(id,idArray,brand,brandArray,color,colorArray,activities,activitiesArray,inventoriesSize,inventoriesSizeArray,categories,categoriesArray
        ,description,name,priceRentalBetween,priceRentalGt,priceRentalGte,priceRentalLt,priceRentalLte,exist,orderPriceRental,page).enqueue(NetworkResponse(listener,1,activity,fragment))
    }

}