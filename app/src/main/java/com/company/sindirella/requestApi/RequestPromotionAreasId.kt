package com.company.sindirella.requestApi

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.*
import com.company.sindirella.response.PromotionArea.PromotionAreaIdResponse
import com.company.sindirella.response.PromotionArea.PromotionAreaResponse
import java.util.ArrayList

class RequestPromotionAreasId(activity: AppCompatActivity?, fragment: Fragment?, id: String?, listener: NetworkResponseListener<PromotionAreaIdResponse>) {

    init {
        val request = RequestCreator.create<Service.PromotionAreasId>(Service.PromotionAreasId::class.java, NetworkSupport.NetworkAdress.base_url)
        request.getPromotionAreasId(id).enqueue(NetworkResponse(listener,1,activity,fragment))
    }

}