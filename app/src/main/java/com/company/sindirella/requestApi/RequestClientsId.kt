package com.company.sindirella.requestApi

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.*
import com.company.sindirella.response.Client.ClientsIdResponse
import com.company.sindirella.response.ClientAddress.PutClientAddressIdResponse
import com.google.gson.JsonObject

class RequestClientsId(activity: AppCompatActivity?, fragment: Fragment?, id: String?, listener: NetworkResponseListener<ClientsIdResponse>) {

    init {
        val request = RequestCreator.create<Service.ClientsId>(Service.ClientsId::class.java, NetworkSupport.NetworkAdress.base_url)
        request.getClientsId(id).enqueue(NetworkResponse(listener,1,activity,fragment))
    }

}