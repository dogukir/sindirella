package com.company.sindirella.requestApi

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.*
import com.company.sindirella.response.Media.GetMediaIdImgUrlOriSizeResponse
import com.company.sindirella.response.Media.GetMediaIdImgUrlResponse

class RequestGetMediaIdImgUrlOriSize(activity: AppCompatActivity?, fragment: Fragment?, id : String?, listener: NetworkResponseListener<GetMediaIdImgUrlOriSizeResponse>) {

    init {
        val request = RequestCreator.create<Service.GetMediaIdImgUrlOriSize>(Service.GetMediaIdImgUrlOriSize::class.java, NetworkSupport.NetworkAdress.base_url)
        request.getMediaIdImgUrlOriSize(id).enqueue(NetworkResponse(listener,1,activity,fragment))
    }

}