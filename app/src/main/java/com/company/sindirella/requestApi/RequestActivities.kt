package com.company.sindirella.requestApi

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.*
import com.company.sindirella.response.Activity.ActivitysResponse
import com.company.sindirella.response.Order.PutOrdersIdResponse
import java.util.ArrayList

class RequestActivities(activity: AppCompatActivity?, fragment: Fragment?, page: Int?, listener: NetworkResponseListener<ArrayList<ActivitysResponse>>) {

    init {
        val request = RequestCreator.create<Service.Activities>(Service.Activities::class.java, NetworkSupport.NetworkAdress.base_url)
        request.getActivities(page).enqueue(NetworkResponse(listener,1,activity,fragment))
    }

}