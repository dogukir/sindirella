package com.company.sindirella.requestApi

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.*
import com.company.sindirella.response.City.CitiesIdResponse
import com.company.sindirella.response.ClientAddress.PostClientAddressResponse
import com.google.gson.JsonObject

class RequestPostClientAddresses(activity: AppCompatActivity?, fragment: Fragment?, body: JsonObject?, listener: NetworkResponseListener<PostClientAddressResponse>) {

    init {
        val request = RequestCreator.create<Service.PostClientAddresses>(Service.PostClientAddresses::class.java, NetworkSupport.NetworkAdress.base_url)
        request.postClientAddresses(body).enqueue(NetworkResponse(listener,1,activity,fragment))
    }

}