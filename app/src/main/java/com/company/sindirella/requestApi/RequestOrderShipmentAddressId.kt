package com.company.sindirella.requestApi

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.*
import com.company.sindirella.response.OrderInvoiceAddress.OrderInvoiceAddressIdResponse
import com.company.sindirella.response.OrderShipmentAddress.OrderShipmentAddressIdResponse

class RequestOrderShipmentAddressId(activity: AppCompatActivity?, fragment: Fragment?, id: String?, listener: NetworkResponseListener<OrderShipmentAddressIdResponse>) {

    init {
        val request = RequestCreator.create<Service.OrderShipmentAddressId>(Service.OrderShipmentAddressId::class.java, NetworkSupport.NetworkAdress.base_url)
        request.getOrderShipmentAddressId(id).enqueue(NetworkResponse(listener,1,activity,fragment))
    }

}