package com.company.sindirella.requestApi

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.*
import com.company.sindirella.response.InvoiceLine.InvoiceLineIdResponse
import com.company.sindirella.response.InvoiceLine.InvoiceLineResponse
import java.util.ArrayList

class RequestInvoiceLinesId(activity: AppCompatActivity?, fragment: Fragment?, id : String?, listener: NetworkResponseListener<InvoiceLineIdResponse>) {

    init {
        val request = RequestCreator.create<Service.InvoiceLinesId>(Service.InvoiceLinesId::class.java, NetworkSupport.NetworkAdress.base_url)
        request.getInvoiceLinesId(id).enqueue(NetworkResponse(listener,1,activity,fragment))
    }

}