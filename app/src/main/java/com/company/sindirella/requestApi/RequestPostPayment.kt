package com.company.sindirella.requestApi

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.*
import com.company.sindirella.response.Payment.GetPaymentResponse
import com.company.sindirella.response.Payment.PostPaymentResponse
import com.google.gson.JsonObject
import java.util.ArrayList

class RequestPostPayment(activity: AppCompatActivity?, fragment: Fragment?, body: JsonObject?, listener: NetworkResponseListener<PostPaymentResponse>) {

    init {
        val request = RequestCreator.create<Service.PostPayment>(Service.PostPayment::class.java, NetworkSupport.NetworkAdress.base_url)
        request.postPayment(body).enqueue(NetworkResponse(listener,1,activity,fragment))
    }

}