package com.company.sindirella.requestApi

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.*
import com.company.sindirella.response.FavProduct.GetFavProductResponse
import com.company.sindirella.response.FavProduct.PostFavProductResponse
import com.google.gson.JsonObject
import java.util.ArrayList

class RequestPostFavProduct(activity: AppCompatActivity?, fragment: Fragment?, body: JsonObject?, listener: NetworkResponseListener<PostFavProductResponse>) {

    init {
        val request = RequestCreator.create<Service.PostFavProduct>(Service.PostFavProduct::class.java, NetworkSupport.NetworkAdress.base_url)
        request.postFavProduct(body).enqueue(NetworkResponse(listener,1,activity,fragment))
    }

}