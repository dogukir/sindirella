package com.company.sindirella.requestApi

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.*
import com.company.sindirella.response.Comment.CommentsIdResponse
import com.company.sindirella.response.Comment.GetCommentsResponse
import java.util.ArrayList

class RequestGetCommentsId(activity: AppCompatActivity?, fragment: Fragment?,id: String?, listener: NetworkResponseListener<CommentsIdResponse>) {

    init {
        val request = RequestCreator.create<Service.GetCommentsId>(Service.GetCommentsId::class.java, NetworkSupport.NetworkAdress.base_url)
        request.getCommentsId(id).enqueue(NetworkResponse(listener,1,activity,fragment)
        )
    }

}