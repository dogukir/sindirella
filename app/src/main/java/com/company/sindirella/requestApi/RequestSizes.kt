package com.company.sindirella.requestApi

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.*
import com.company.sindirella.response.Size.SizeResponse
import com.company.sindirella.response.SizeGroup.SizeGroupResponse
import retrofit2.http.Query
import java.util.ArrayList

class RequestSizes(activity: AppCompatActivity?, fragment: Fragment?, country: String?, countryArray: ArrayList<String>?,page: Int?, listener: NetworkResponseListener<ArrayList<SizeResponse>>) {

    init {
        val request = RequestCreator.create<Service.Sizes>(Service.Sizes::class.java, NetworkSupport.NetworkAdress.base_url)
        request.getSizes(country,countryArray,page).enqueue(NetworkResponse(listener,1,activity,fragment))
    }

}