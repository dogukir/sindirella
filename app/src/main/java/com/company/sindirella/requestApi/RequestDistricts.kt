package com.company.sindirella.requestApi

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.*
import com.company.sindirella.response.Country.CountriesIdResponse
import com.company.sindirella.response.District.DistrictResponse
import java.util.ArrayList

class RequestDistricts(activity: AppCompatActivity?, fragment: Fragment?, page: Int?, listener: NetworkResponseListener<ArrayList<DistrictResponse>>) {

    init {
        val request = RequestCreator.create<Service.Districts>(Service.Districts::class.java, NetworkSupport.NetworkAdress.base_url)
        request.getDistricts(page).enqueue(NetworkResponse(listener,1,activity,fragment))
    }

}