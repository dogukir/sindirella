package com.company.sindirella.requestApi

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.*
import com.company.sindirella.response.Order.OrderHistoryResponse
import com.company.sindirella.response.Order.OrdersResponse
import com.google.gson.JsonObject
import retrofit2.http.Body
import java.util.ArrayList

class RequestOrders(activity: AppCompatActivity?, fragment: Fragment?, body: JsonObject?, listener: NetworkResponseListener<OrdersResponse>) {

    init {
        val request = RequestCreator.create<Service.Orders>(Service.Orders::class.java, NetworkSupport.NetworkAdress.base_url)
        request.postOrders(body).enqueue(NetworkResponse(listener,1,activity,fragment))
    }

}