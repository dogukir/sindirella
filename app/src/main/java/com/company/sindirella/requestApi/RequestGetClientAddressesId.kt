package com.company.sindirella.requestApi

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.*
import com.company.sindirella.response.ClientAddress.GetClientAddressIdResponse
import com.company.sindirella.response.ClientAddress.GetClientAddressResponse
import java.util.ArrayList

class RequestGetClientAddressesId(activity: AppCompatActivity?, fragment: Fragment?, id: String?, listener: NetworkResponseListener<GetClientAddressIdResponse>) {

    init {
        val request = RequestCreator.create<Service.GetClientAddressesId>(Service.GetClientAddressesId::class.java, NetworkSupport.NetworkAdress.base_url)
        request.getClientAddressesId(id).enqueue(NetworkResponse(listener,1,activity,fragment))
    }

}