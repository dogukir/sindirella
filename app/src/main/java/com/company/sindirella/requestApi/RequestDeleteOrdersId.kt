package com.company.sindirella.requestApi

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.*
import com.company.sindirella.response.Order.PutOrdersIdResponse
import com.google.gson.JsonObject

class RequestDeleteOrdersId(activity: AppCompatActivity?, fragment: Fragment?, id: String?, listener: NetworkResponseListener<PutOrdersIdResponse>) {

    init {
        val request = RequestCreator.create<Service.DeleteOrdersId>(Service.DeleteOrdersId::class.java, NetworkSupport.NetworkAdress.base_url)
        request.deleteOrdersId(id).enqueue(NetworkResponse(listener,1,activity,fragment))
    }

}