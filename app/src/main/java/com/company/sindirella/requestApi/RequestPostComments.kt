package com.company.sindirella.requestApi

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.*
import com.company.sindirella.response.Color.ColorsIdResponse
import com.company.sindirella.response.Comment.PostCommentsResponse
import com.google.gson.JsonObject

class RequestPostComments(activity: AppCompatActivity?, fragment: Fragment?, body: JsonObject?, listener: NetworkResponseListener<PostCommentsResponse>) {

    init {
        val request = RequestCreator.create<Service.PostComments>(Service.PostComments::class.java, NetworkSupport.NetworkAdress.base_url)
        request.postComments(body).enqueue(NetworkResponse(listener,1,activity,fragment))
    }

}