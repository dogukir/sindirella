package com.company.sindirella.requestApi

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.*
import com.company.sindirella.response.District.DistrictIdResponse
import com.company.sindirella.response.FavProduct.GetFavProductResponse
import java.util.ArrayList

class RequestGetFavProduct(activity: AppCompatActivity?, fragment: Fragment?, page: Int?, listener: NetworkResponseListener<ArrayList<GetFavProductResponse>>) {

    init {
        val request = RequestCreator.create<Service.GetFavProduct>(Service.GetFavProduct::class.java, NetworkSupport.NetworkAdress.base_url)
        request.getFavProduct(page).enqueue(NetworkResponse(listener,1,activity,fragment))
    }

}