package com.company.sindirella.requestApi

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.*
import com.company.sindirella.response.Invoice.InvoiceIdResponse
import com.company.sindirella.response.Media.PostMediaResponse

class RequestPostMedia(activity: AppCompatActivity?, fragment: Fragment?, imageFile : String?, listener: NetworkResponseListener<PostMediaResponse>) {

    init {
        val request = RequestCreator.create<Service.PostMedia>(Service.PostMedia::class.java, NetworkSupport.NetworkAdress.base_url)
        request.postMedia(imageFile).enqueue(NetworkResponse(listener,1,activity,fragment))
    }

}