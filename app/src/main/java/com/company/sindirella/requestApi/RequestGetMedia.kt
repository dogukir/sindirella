package com.company.sindirella.requestApi

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.*
import com.company.sindirella.response.Media.GetMediaResponse
import com.company.sindirella.response.Media.PostMediaResponse
import java.util.ArrayList

class RequestGetMedia(activity: AppCompatActivity?, fragment: Fragment?, page : Int?, listener: NetworkResponseListener<ArrayList<GetMediaResponse>>) {

    init {
        val request = RequestCreator.create<Service.GetMedia>(Service.GetMedia::class.java, NetworkSupport.NetworkAdress.base_url)
        request.getMedia(page).enqueue(NetworkResponse(listener,1,activity,fragment))
    }

}