package com.company.sindirella.requestApi

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.*
import com.company.sindirella.response.Comment.CommentsIdResponse
import com.company.sindirella.response.Country.CountriesResponse
import java.util.ArrayList

class RequestCountries(activity: AppCompatActivity?, fragment: Fragment?, page: Int?, listener: NetworkResponseListener<ArrayList<CountriesResponse>>) {

    init {
        val request = RequestCreator.create<Service.Countries>(Service.Countries::class.java, NetworkSupport.NetworkAdress.base_url)
        request.getCountries(page).enqueue(NetworkResponse(listener,1,activity,fragment))
    }

}