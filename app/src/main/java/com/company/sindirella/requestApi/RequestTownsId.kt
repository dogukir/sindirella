package com.company.sindirella.requestApi

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.*
import com.company.sindirella.response.Town.TownIdResponse
import com.company.sindirella.response.Town.TownResponse
import java.util.ArrayList

class RequestTownsId(activity: AppCompatActivity?, fragment: Fragment?, id: String?, listener: NetworkResponseListener<TownIdResponse>) {

    init {
        val request = RequestCreator.create<Service.TownsId>(Service.TownsId::class.java, NetworkSupport.NetworkAdress.base_url)
        request.getTownsId(id).enqueue(NetworkResponse(listener,1,activity,fragment))
    }

}