package com.company.sindirella.requestApi

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.*
import com.company.sindirella.response.Tag.TagIdResponse
import com.company.sindirella.response.Town.TownResponse
import java.util.ArrayList

class RequestTowns(activity: AppCompatActivity?, fragment: Fragment?, page: Int?, listener: NetworkResponseListener<ArrayList<TownResponse>>) {

    init {
        val request = RequestCreator.create<Service.Towns>(Service.Towns::class.java, NetworkSupport.NetworkAdress.base_url)
        request.getTowns(page).enqueue(NetworkResponse(listener,1,activity,fragment))
    }

}