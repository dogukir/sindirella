package com.company.sindirella.requestApi

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.*
import com.company.sindirella.response.Order.ActiveOrdersResponse
import com.company.sindirella.response.Order.OrderHistoryResponse
import java.util.ArrayList

class RequestOrderHistory(activity: AppCompatActivity?, fragment: Fragment?, id: Int?, idArray: ArrayList<Int>?, status: Int?, statusArray: ArrayList<Int>?, orderId : String?, page : Int?, listener: NetworkResponseListener<ArrayList<OrderHistoryResponse>>) {

    init {
        val request = RequestCreator.create<Service.OrderHistory>(Service.OrderHistory::class.java, NetworkSupport.NetworkAdress.base_url)
        request.getOrderHistory(id,idArray,status,statusArray,orderId,page).enqueue(NetworkResponse(listener,1,activity,fragment))
    }

}