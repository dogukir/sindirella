package com.company.sindirella.requestApi

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.*
import com.company.sindirella.response.Order.GetOrdersIdResponse
import com.company.sindirella.response.Order.OrdersResponse
import com.google.gson.JsonObject

class RequestGetOrdersId(activity: AppCompatActivity?, fragment: Fragment?, id: String?, listener: NetworkResponseListener<GetOrdersIdResponse>) {

    init {
        val request = RequestCreator.create<Service.GetOrdersId>(Service.GetOrdersId::class.java, NetworkSupport.NetworkAdress.base_url)
        request.getOrdersId(id).enqueue(NetworkResponse(listener,1,activity,fragment))
    }

}