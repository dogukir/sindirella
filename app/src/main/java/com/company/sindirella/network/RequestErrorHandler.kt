package com.company.sindirella.network

import org.json.JSONException
import org.json.JSONObject
import java.io.IOException

class RequestErrorHandler(errorBody: okhttp3.ResponseBody?) {

    private var obj: JSONObject? = null
    private var error = ""

    val errorString: String
        get() {

            try {

                if (obj != null) {

                    error = obj!!.getString("error")

                }

            } catch (e: JSONException) {

                e.printStackTrace()

            }

            return error

        }

    init {

        try {

            if (errorBody != null) {

                val body = errorBody.string()

                if (body.length != 0) {

                    obj = JSONObject(body)

                } else {

                    obj = null

                }

            } else {

                obj = null

            }


        } catch (e: IOException) {

            e.printStackTrace()

        } catch (e: JSONException) {
            e.printStackTrace()
        }

    }

}