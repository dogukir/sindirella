package com.company.sindirella.network

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.google.gson.JsonSyntaxException
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class NetworkResponse<ResponseType>(private val listener: NetworkResponseListener<ResponseType>, private val type: Int, private val activity: AppCompatActivity?, private val fragment: Fragment?) :
    Callback<ResponseType> {
    private var error = ""
    private  val OK = NetworkSupport.HttpCodes.OK
    private  val CREATED = NetworkSupport.HttpCodes.CREATED
    private  val CONFLICT = NetworkSupport.HttpCodes.CONFLICT

    private fun sendSuccessCreateConflict(response: Response<ResponseType>) {

        if (response.code() == OK || response.code() == CREATED || response.code() == CONFLICT) {

            response.body()?.let { listener.onResponseReceived(it) }

        } else {

            val errorHandler = RequestErrorHandler(response.errorBody())
            error = errorHandler.errorString

            if (error == "") {

                listener.onError(response.code(), error)

            } else {

                listener.onError(response.code(), error)

            }

        }

    }

    private fun sendSuccessCreate(response: Response<ResponseType>) {

        if (response.code() == OK || response.code() == CREATED) {

            response.body()?.let { listener.onResponseReceived(it) }

        } else {

            val errorHandler = RequestErrorHandler(response.errorBody())
            error = errorHandler.errorString

            if (error == "") {

                listener.onError(response.code(), error)

            } else {

                listener.onError(response.code(), error)

            }

        }

    }

    private fun sendSuccess(response: Response<ResponseType>) {

        if (response.code() == OK || response.code() == CREATED) {

            response.body()?.let { listener.onResponseReceived(it) }

        } else {

            val errorHandler = RequestErrorHandler(response.errorBody())
            error = errorHandler.errorString

            if (error == "") {

                listener.onError(response.code(), error)

            } else {

                listener.onError(response.code(), error)

            }

        }

    }

    private fun sendFail(t: Throwable) {

        if (t is JsonSyntaxException) {

            listener.onEmptyResponse(null)

        } else {

            listener.onError(-1, error)

        }

    }

    private fun successLogic(response: Response<ResponseType>) {

        if (type == 1) {

            sendSuccess(response)

        } else if (type == 2) {

            sendSuccessCreate(response)

        } else {

            sendSuccessCreateConflict(response)

        }

    }

    override fun onResponse(call: Call<ResponseType>, response: Response<ResponseType>) {

        if (activity != null) {

            if (!activity.isFinishing) {

                successLogic(response)

            }

        } else if (fragment != null) {

            if (fragment.isAdded) {

                successLogic(response)

            }

        }

    }

    override fun onFailure(call: Call<ResponseType>, t: Throwable) {

        if (activity != null) {

            if (!activity.isFinishing) {

                sendFail(t)

            }

        } else if (fragment != null) {

            if (fragment.isAdded) {

                sendFail(t)

            }

        }

    }

}