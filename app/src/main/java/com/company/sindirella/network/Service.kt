package com.company.sindirella.network

import com.company.sindirella.response.Activity.ActivitysIdResponse
import com.company.sindirella.response.Activity.ActivitysResponse
import com.company.sindirella.response.BasketItem.BasketItemsIdResponse
import com.company.sindirella.response.Brand.BrandsIdNameResponse
import com.company.sindirella.response.Brand.BrandsIdResponse
import com.company.sindirella.response.Brand.BrandsResponse
import com.company.sindirella.response.Category.CategoriesIdResponse
import com.company.sindirella.response.Category.CategoriesResponse
import com.company.sindirella.response.City.CitiesIdResponse
import com.company.sindirella.response.City.CitiesResponse
import com.company.sindirella.response.Client.*
import com.company.sindirella.response.ClientAddress.GetClientAddressIdResponse
import com.company.sindirella.response.ClientAddress.GetClientAddressResponse
import com.company.sindirella.response.ClientAddress.PostClientAddressResponse
import com.company.sindirella.response.ClientAddress.PutClientAddressIdResponse
import com.company.sindirella.response.Color.ColorsIdResponse
import com.company.sindirella.response.Color.ColorsResponse
import com.company.sindirella.response.Comment.CommentsIdResponse
import com.company.sindirella.response.Comment.GetCommentsResponse
import com.company.sindirella.response.Comment.PostCommentsResponse
import com.company.sindirella.response.Country.CountriesIdResponse
import com.company.sindirella.response.Country.CountriesResponse
import com.company.sindirella.response.District.DistrictIdResponse
import com.company.sindirella.response.District.DistrictResponse
import com.company.sindirella.response.FavProduct.FavProductIdResponse
import com.company.sindirella.response.FavProduct.GetFavProductResponse
import com.company.sindirella.response.FavProduct.PostFavProductResponse
import com.company.sindirella.response.HelpPage.HelpPageIdResponse
import com.company.sindirella.response.HelpPage.HelpPageResponse
import com.company.sindirella.response.Invertory.InventoriesIdResponse
import com.company.sindirella.response.Invertory.InventoriesResponse
import com.company.sindirella.response.Invoice.InvoiceIdResponse
import com.company.sindirella.response.Invoice.InvoiceResponse
import com.company.sindirella.response.InvoiceLine.InvoiceLineIdResponse
import com.company.sindirella.response.InvoiceLine.InvoiceLineResponse
import com.company.sindirella.response.Media.*
import com.company.sindirella.response.Order.*
import com.company.sindirella.response.OrderDetail.GetOrderDetailIdResponse
import com.company.sindirella.response.OrderDetail.PostOrderDetailResponse
import com.company.sindirella.response.OrderInvoiceAddress.OrderInvoiceAddressIdResponse
import com.company.sindirella.response.OrderShipmentAddress.OrderShipmentAddressIdResponse
import com.company.sindirella.response.Payment.GetPaymentIdResponse
import com.company.sindirella.response.Payment.GetPaymentResponse
import com.company.sindirella.response.Payment.PostPaymentResponse
import com.company.sindirella.response.Product.ProductIdResponse
import com.company.sindirella.response.Product.ProductResponse
import com.company.sindirella.response.PromotionArea.PromotionAreaIdResponse
import com.company.sindirella.response.PromotionArea.PromotionAreaResponse
import com.company.sindirella.response.Size.SizeIdResponse
import com.company.sindirella.response.Size.SizeResponse
import com.company.sindirella.response.SizeGroup.SizeGroupResponse
import com.company.sindirella.response.Tag.TagIdResponse
import com.company.sindirella.response.Tag.TagResponse
import com.company.sindirella.response.Town.TownIdResponse
import com.company.sindirella.response.Town.TownResponse
import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.http.*
import java.util.ArrayList

interface Service {

    //Order

    interface ActiveOrders {
        @Headers("Content-Type: application/json","accept: application/json")
        @GET("active_orders")
        fun getActiveOrders(@Query("id") id: Int?,@Query("id[]") idArray: ArrayList<Int>?,@Query("status") status: Int?,@Query("status[]") statusArray: ArrayList<Int>?,
                            @Query("order[id]") orderId : String?,@Query("page") page : Int?) : Call<ArrayList<ActiveOrdersResponse>>
    }

    interface OrderHistory {
        @Headers("Content-Type: application/json","accept: application/json")
        @GET("order_history")
        fun getOrderHistory(@Query("id") id: Int?,@Query("id[]") idArray: ArrayList<Int>?,@Query("status") status: Int?,@Query("status[]") statusArray: ArrayList<Int>?,
                            @Query("order[id]") orderId : String?,@Query("page") page : Int?) : Call<ArrayList<OrderHistoryResponse>>
    }

    interface Orders {
        @Headers("Content-Type: application/json","accept: application/json")
        @POST("orders")
        fun postOrders(@Body body: JsonObject?) : Call<OrdersResponse>
    }

    interface GetOrdersId {
        @Headers("Content-Type: application/json","accept: application/json")
        @GET("orders/{id}")
        fun getOrdersId(@Path("id") id: String?) : Call<GetOrdersIdResponse>
    }

    interface PutOrdersId {
        @Headers("Content-Type: application/json","accept: application/json")
        @PUT("orders/{id}")
        fun putOrdersId(@Path("id") id: String?, @Body body: JsonObject?) : Call<PutOrdersIdResponse>
    }

    interface DeleteOrdersId {
        @Headers("Content-Type: application/json","accept: application/json")
        @DELETE("orders/{id}")
        fun deleteOrdersId(@Path("id") id: String?) : Call<PutOrdersIdResponse> // Delete işlemi kontrol edilecek!!
    }


    // Activity

    interface Activities {
        @Headers("Content-Type: application/json","accept: application/json")
        @GET("activities")
        fun getActivities(@Query("page") page: Int?) : Call<ArrayList<ActivitysResponse>>
    }

    interface ActivitiesId {
        @Headers("Content-Type: application/json","accept: application/json")
        @GET("activities/{id}")
        fun getActivitiesId(@Path("id") id: String?) : Call<ActivitysIdResponse>
    }


    // BasketItem

    interface BasketItemId {
        @Headers("Content-Type: application/json","accept: application/json")
        @GET("basket_items/{id}")
        fun getBasketItemId(@Path("id") id: String?) : Call<BasketItemsIdResponse>
    }

    // Brand

    interface Brands {
        @Headers("Content-Type: application/json","accept: application/json")
        @GET("brands")
        fun getBrands(@Query("page") page: Int?) : Call<ArrayList<BrandsResponse>>
    }

    interface BrandsId {
        @Headers("Content-Type: application/json","accept: application/json")
        @GET("brands/{id}")
        fun getBrandsId(@Path("id") id: String?) : Call<BrandsIdResponse>
    }

    interface BrandsIdName {
        @Headers("Content-Type: application/json","accept: application/json")
        @GET("brands/{id}/name")
        fun getBrandsIdName(@Path("id") id: String?) : Call<BrandsIdNameResponse>
    }

    // Category

    interface Categories {
        @Headers("Content-Type: application/json","accept: application/json")
        @GET("categories")
        fun getCategories(@Query("exist[parent]") exist: Boolean?,@Query("page") page: Int?) : Call<ArrayList<CategoriesResponse>>
    }

    interface CategoriesId {
        @Headers("Content-Type: application/json","accept: application/json")
        @GET("categories/{id}")
        fun getCategoriesId(@Path("id") id: String?) : Call<CategoriesIdResponse>
    }

    // City

    interface Cities {
        @Headers("Content-Type: application/json","accept: application/json")
        @GET("cities")
        fun getCities(@Query("order[sortOrder]") orderSort: String?,@Query("order[name]") orderName: String?,@Query("page") page: Int?) : Call<ArrayList<CitiesResponse>>
    }

    interface CitiesId {
        @Headers("Content-Type: application/json","accept: application/json")
        @GET("cities/{id}")
        fun getCitiesId(@Path("id") id: String?) : Call<CitiesIdResponse>
    }

    // ClientAddress

    interface PostClientAddresses {
        @Headers("Content-Type: application/json","accept: application/json")
        @POST("client_addresses")
        fun postClientAddresses(@Body body: JsonObject?) : Call<PostClientAddressResponse>
    }

    interface GetClientAddresses {
        @Headers("Content-Type: application/json","accept: application/json")
        @POST("client_addresses")
        fun getClientAddresses(@Query("page") page: Int?) : Call<ArrayList<GetClientAddressResponse>>
    }

    interface GetClientAddressesId {
        @Headers("Content-Type: application/json","accept: application/json")
        @POST("client_addresses/{id}")
        fun getClientAddressesId(@Path("id") id: String?) : Call<GetClientAddressIdResponse>
    }

    interface PutClientAddressesId {
        @Headers("Content-Type: application/json","accept: application/json")
        @POST("client_addresses/{id}")
        fun putClientAddressesId(@Path("id") id: String?,@Body body: JsonObject?) : Call<PutClientAddressIdResponse>
    }

    interface DeleteClientAddressesId {
        @Headers("Content-Type: application/json","accept: application/json")
        @DELETE("client_addresses/{id}")
        fun deleteClientAddressesId(@Path("id") id: String?)
    }


    // Client

    interface ClientsId {
        @Headers("Content-Type: application/json","accept: application/json")
        @GET("clients/{id}")
        fun getClientsId(@Path("id") id: String?) : Call<ClientsIdResponse>
    }

    interface Google {
        @Headers("Content-Type: application/json","accept: application/json")
        @GET("google")
        fun postGoogle(@Body body: JsonObject?) : Call<PostGoogleResponse>
    }

    interface Login {
        @Headers("Content-Type: application/json","accept: application/json")
        @POST("login")
        fun postLogin(@Body body: JsonObject?) : Call<PostLoginResponse>
    }

    interface GetMe {
        @Headers("Content-Type: application/json","accept: application/json")
        @GET("me")
        fun getMe() : Call<GetMeResponse>
    }

    interface PutMe {
        @Headers("Content-Type: application/json","accept: application/json")
        @PUT("me")
        fun putMe(@Query("id") id: String?,@Body body: JsonObject?) : Call<PutMeResponse> // id path olabilir !!!
    }

    interface PutPassword {
        @Headers("Content-Type: application/json","accept: application/json")
        @PUT("password")
        fun putPassword(@Query("id") id: String?,@Body body: JsonObject?) : Call<PutPasswordResponse> // id path olabilir !!!
    }

    interface Register {
        @Headers("Content-Type: application/json","accept: application/json")
        @POST("register")
        fun postRegister(@Body body: JsonObject?) : Call<PostRegisterResponse>
    }

    // Color

    interface Colors {
        @Headers("Content-Type: application/json","accept: application/json")
        @GET("colors")
        fun getColors(@Query("page") page : Int?) : Call<ArrayList<ColorsResponse>>
    }

    interface ColorsId {
        @Headers("Content-Type: application/json","accept: application/json")
        @GET("colors/{id}")
        fun getColorsId(@Path("id") id : String?) : Call<ColorsIdResponse>
    }

    // Comment

    interface PostComments {
        @Headers("Content-Type: application/json","accept: application/json")
        @POST("comments")
        fun postComments(@Body body: JsonObject?) : Call<PostCommentsResponse>
    }

    interface GetComments {
        @Headers("Content-Type: application/json","accept: application/json")
        @GET("comments")
        fun getComments(@Query("exist[medias]") exist: Boolean?,@Query("hasTitle") hasTitle: Boolean?,@Query("hasTitle[]") hasTitleArray: ArrayList<Boolean>?,
                        @Query("isActive") isActive: Boolean?,@Query("isActive[]") isActiveArray: ArrayList<Boolean>?,@Query("product") product: String?,
                        @Query("product[]") productArray: ArrayList<String>?,@Query("order[id]") orderId: String?,@Query("page") page: Int?) : Call<ArrayList<GetCommentsResponse>>
    }

    interface GetCommentsId {
        @Headers("Content-Type: application/json","accept: application/json")
        @GET("comments/{id}")
        fun getCommentsId(@Path("id") id: String?) : Call<CommentsIdResponse>
    }

    // Country

    interface Countries {
        @Headers("Content-Type: application/json","accept: application/json")
        @GET("countries")
        fun getCountries(@Query("page") page: Int?) : Call<ArrayList<CountriesResponse>>
    }

    interface CountriesId {
        @Headers("Content-Type: application/json","accept: application/json")
        @GET("countries/{id}")
        fun getCountriesId(@Path("id") id: String?) : Call<CountriesIdResponse>
    }

    // District

    interface Districts {
        @Headers("Content-Type: application/json","accept: application/json")
        @GET("districts")
        fun getDistricts(@Query("page") page: Int?) : Call<ArrayList<DistrictResponse>>
    }

    interface DistrictsId {
        @Headers("Content-Type: application/json","accept: application/json")
        @GET("districts/{id}")
        fun getDistrictsId(@Path("id") id: String?) : Call<DistrictIdResponse>
    }

    // FavProduct

    interface GetFavProduct {
        @Headers("Content-Type: application/json","accept: application/json")
        @GET("fav_products")
        fun getFavProduct(@Query("page") page: Int?) : Call<ArrayList<GetFavProductResponse>>
    }

    interface PostFavProduct {
        @Headers("Content-Type: application/json","accept: application/json")
        @POST("fav_products")
        fun postFavProduct(@Body body: JsonObject?) : Call<PostFavProductResponse>
    }

    interface GetFavProductId {
        @Headers("Content-Type: application/json","accept: application/json")
        @GET("fav_products/{id}")
        fun getFavProductId(@Path("id") id: String?) : Call<FavProductIdResponse>
    }

    interface DeleteFavProductId {
        @Headers("Content-Type: application/json","accept: application/json")
        @DELETE("fav_products/{id}")
        fun deleteFavProductId(@Path("id") id: String?)
    }

    // HelpPage

    interface HelpPages {
        @Headers("Content-Type: application/json","accept: application/json")
        @GET("help_pages")
        fun getHelpPages(@Query("code") code: String?,@Query("code[]") codeArray: ArrayList<String>?,@Query("page") page: Int?) : Call<ArrayList<HelpPageResponse>>
    }

    interface HelpPagesId {
        @Headers("Content-Type: application/json","accept: application/json")
        @GET("help_pages/{id}")
        fun getHelpPagesId(@Path("id") id: String?) : Call<HelpPageIdResponse>
    }

    // Invertory

    interface Inventories {
        @Headers("Content-Type: application/json","accept: application/json")
        @GET("inventories")
        fun getInventories(@Query("id") id: Int?,@Query("id[]") idArray: ArrayList<Int>?,@Query("size") size: String?,
                           @Query("size[]") sizeArray: ArrayList<String>?,@Query("page") page: Int?) : Call<ArrayList<InventoriesResponse>>
    }

    interface InventoriesId {
        @Headers("Content-Type: application/json","accept: application/json")
        @GET("inventories/{id}")
        fun getInventoriesId(@Path("id") id: String?) : Call<InventoriesIdResponse>
    }

    // InvoiceLine

    interface InvoiceLines {
        @Headers("Content-Type: application/json","accept: application/json")
        @GET("invoice_lines")
        fun getInvoiceLines(@Query("page") page: Int?) : Call<ArrayList<InvoiceLineResponse>>
    }

    interface InvoiceLinesId {
        @Headers("Content-Type: application/json","accept: application/json")
        @GET("invoice_lines/{id}")
        fun getInvoiceLinesId(@Path("id") id: String?) : Call<InvoiceLineIdResponse>
    }

    // Invoice

    interface Invoices {
        @Headers("Content-Type: application/json","accept: application/json")
        @GET("invoices")
        fun getInvoices(@Query("page") page: Int?) : Call<ArrayList<InvoiceResponse>>
    }

    interface InvoicesId {
        @Headers("Content-Type: application/json","accept: application/json")
        @GET("invoices/{id}")
        fun getInvoicesId(@Path("id") id: String?) : Call<InvoiceIdResponse>
    }

    // Media

    interface PostMedia {
        @Headers("Content-Type: application/json","accept: application/json")
        @Multipart
        @POST("media")
        fun postMedia(@Part("imageFile") imageFile: String?) : Call<PostMediaResponse>
    }

    interface GetMedia {
        @Headers("Content-Type: application/json","accept: application/json")
        @GET("media")
        fun getMedia(@Query("page") page: Int?) : Call<ArrayList<GetMediaResponse>>
    }

    interface GetMediaId {
        @Headers("Content-Type: application/json","accept: application/json")
        @GET("media/{id}")
        fun getMediaId(@Path("id") id: String?) : Call<GetMediaIdResponse>
    }

    interface GetMediaIdImgUrl {
        @Headers("Content-Type: application/json","accept: application/json")
        @GET("media/{id}/image_url")
        fun getMediaIdImgUrl(@Path("id") id: String?) : Call<GetMediaIdImgUrlResponse>
    }

    interface GetMediaIdImgUrlOriSize {
        @Headers("Content-Type: application/json","accept: application/json")
        @GET("media/{id}/image_url/origin_size_url")
        fun getMediaIdImgUrlOriSize(@Path("id") id: String?) : Call<GetMediaIdImgUrlOriSizeResponse>
    }

    interface GetMediaIdOriSize {
        @Headers("Content-Type: application/json","accept: application/json")
        @GET("media/{id}/origin_size_url")
        fun getMediaIdOriSize(@Path("id") id: String?) : Call<GetMediaIdOriSizeResponse>
    }

    interface GetMediaIdOriSizeImgUrl {
        @Headers("Content-Type: application/json","accept: application/json")
        @GET("media/{id}/origin_size_url/image_url")
        fun getMediaIdOriSizeImgUrl(@Path("id") id: String?) : Call<GetMediaIdOriSizeImgUrlResponse>
    }

    // OrderDetail

    interface GetOrderDetails {
        @Headers("Content-Type: application/json","accept: application/json")
        @GET("order_details")
        fun getOrderDetails(@Query("id") id: Int?,@Query("id[]") idArray: ArrayList<Int>?,@Query("status") status: Int?,
                            @Query("status[]") statusArray: ArrayList<Int>?,@Query("order[id]") orderId: String?,
                            @Query("page") page: Int?) : Call<ArrayList<GetOrderDetails>>
    }

    interface PostOrderDetails {
        @Headers("Content-Type: application/json","accept: application/json")
        @POST("order_details")
        fun postOrderDetails(@Body body: JsonObject?) : Call<PostOrderDetailResponse>
    }

    interface GetOrderDetailsId {
        @Headers("Content-Type: application/json","accept: application/json")
        @GET("order_details/{id}")
        fun getOrderDetailsId(@Path("id") id: String?) : Call<GetOrderDetailIdResponse>
    }

    interface DeleteOrderDetailsId {
        @Headers("Content-Type: application/json","accept: application/json")
        @DELETE("order_details/{id}")
        fun deleteOrderDetailsId(@Path("id") id: String?)
    }

    //OrderInvoiceAddress

    interface OrderInvoiceAddressId {
        @Headers("Content-Type: application/json","accept: application/json")
        @GET("order_invoice_addresses/{id}")
        fun getOrderInvoiceAddressId(@Path("id") id: String?) : Call<OrderInvoiceAddressIdResponse>
    }

    //OrderShipmentAddress

    interface OrderShipmentAddressId {
        @Headers("Content-Type: application/json","accept: application/json")
        @GET("order_shipment_addresses/{id}")
        fun getOrderShipmentAddressId(@Path("id") id: String?) : Call<OrderShipmentAddressIdResponse>
    }

    // Payment

    interface GetPayment {
        @Headers("Content-Type: application/json","accept: application/json")
        @GET("payments")
        fun getPayment(@Query("page") page: Int?) : Call<ArrayList<GetPaymentResponse>>
    }

    interface PostPayment {
        @Headers("Content-Type: application/json","accept: application/json")
        @POST("payments")
        fun postPayment(@Body body: JsonObject?) : Call<PostPaymentResponse>
    }

    interface GetPaymentId {
        @Headers("Content-Type: application/json","accept: application/json")
        @GET("payments/{id}")
        fun getPaymentId(@Path("id") id: String?) : Call<GetPaymentIdResponse>
    }

    // Product

    interface Product {
        @Headers("Content-Type: application/json","accept: application/json")
        @GET("products")
        fun getProduct(@Query("id") id: Int?,@Query("id[]") idArray: ArrayList<Int>?,@Query("brand") brand: String?,
                       @Query("brand[]") brandArray: ArrayList<String>?,@Query("color") color: String?,@Query("color[]") colorArray: ArrayList<String>?,
                       @Query("activities") activities: String?,@Query("activities[]") activitiesArray: ArrayList<String>?,
                       @Query("inventories.size") inventoriesSize: String?,@Query("inventories.size[]") inventoriesSizeArray: ArrayList<String>?,
                       @Query("categories") categories: String?,@Query("categories[]") categoriesArray: ArrayList<String>?,
                       @Query("description") description: String?,@Query("name") name: String?,@Query("priceRental[between]") priceRentalBetween: String?,
                       @Query("priceRental[gt]") priceRentalGt: String?,@Query("priceRental[gte]") priceRentalGte: String?,
                       @Query("priceRental[lt]") priceRentalLt: String?,@Query("priceRental[lte]") priceRentalLte: String?,
                       @Query("exists[pictures]") exist: Boolean?,@Query("order[priceRental]") orderPriceRental: String?,
                       @Query("page") page: Int?) : Call<ArrayList<ProductResponse>>
    }

    interface ProductId {
        @Headers("Content-Type: application/json","accept: application/json")
        @GET("products/{id}")
        fun getProductId(@Path("id") id: String?) : Call<ProductIdResponse>
    }

    // PromotionArea

    interface PromotionAreas {
        @Headers("Content-Type: application/json","accept: application/json")
        @GET("promotion_areas")
        fun getPromotionAreas(@Query("page") page: Int?) : Call<ArrayList<PromotionAreaResponse>>
    }

    interface PromotionAreasId {
        @Headers("Content-Type: application/json","accept: application/json")
        @GET("promotion_areas/{id}")
        fun getPromotionAreasId(@Path("id") id: String?) : Call<PromotionAreaIdResponse>
    }

    // SizeGroup

    interface SizeGroup {
        @Headers("Content-Type: application/json","accept: application/json")
        @GET("size_groups")
        fun getSizeGroup(@Query("page") page: Int?) : Call<ArrayList<SizeGroupResponse>>
    }

    // Size

    interface Sizes {
        @Headers("Content-Type: application/json","accept: application/json")
        @GET("sizes")
        fun getSizes(@Query("country") country: String?,@Query("country[]") countryArray: ArrayList<String>?,@Query("page") page: Int?) : Call<ArrayList<SizeResponse>>
    }

    interface SizesId {
        @Headers("Content-Type: application/json","accept: application/json")
        @GET("sizes/{id}")
        fun getSizesId(@Path("id") id: String?) : Call<SizeIdResponse>
    }

    // Tag

    interface Tags {
        @Headers("Content-Type: application/json","accept: application/json")
        @GET("tags")
        fun getTags(@Query("page") page: Int?) : Call<ArrayList<TagResponse>>
    }

    interface TagsId {
        @Headers("Content-Type: application/json","accept: application/json")
        @GET("tags/{id}")
        fun getTagsId(@Path("id") id: String?) : Call<TagIdResponse>
    }

    // Town

    interface Towns {
        @Headers("Content-Type: application/json","accept: application/json")
        @GET("towns")
        fun getTowns(@Query("page") page: Int?) : Call<ArrayList<TownResponse>>
    }

    interface TownsId {
        @Headers("Content-Type: application/json","accept: application/json")
        @GET("towns/{id}")
        fun getTownsId(@Path("id") id: String?) : Call<TownIdResponse>
    }

}

