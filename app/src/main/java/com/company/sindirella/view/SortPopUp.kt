package com.company.sindirella.view

import android.app.Dialog
import android.content.Context
import android.view.LayoutInflater
import android.view.WindowManager
import android.widget.Button
import android.widget.LinearLayout
import android.widget.RelativeLayout
import androidx.core.content.ContextCompat
import com.company.sindirella.R
import java.util.ArrayList
import top.defaults.drawabletoolbox.DrawableBuilder

class SortPopUp(context: Context,private val data : ArrayList<String>) : Dialog(context, R.style.Theme_AppCompat_Light_Dialog) {

    private var sortLayout : RelativeLayout? = null
    var exitButton : Button? = null
    private var mainLayout : LinearLayout? = null


    fun createView(layout: Int){

        if (layout == R.layout.sort_pop_up){

            val view = LayoutInflater.from(context).inflate(layout,null)

            sortLayout = view.findViewById(R.id.sortLayout)
            exitButton = view.findViewById(R.id.exitButton)
            mainLayout = view.findViewById(R.id.mainLayout)



            this.window?.setBackgroundDrawable(ContextCompat.getDrawable(context, R.color.transparent))
            this.window?.clearFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND)
            this.getWindow()?.setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT)

            mainLayout?.background = DrawableBuilder().solidColor(ContextCompat.getColor(context,R.color.transparent)).cornerRadius(50).build()
            sortLayout?.background = DrawableBuilder().solidColor(ContextCompat.getColor(context,R.color.view_background)).cornerRadius(50).build()
            exitButton?.background = DrawableBuilder().solidColor(ContextCompat.getColor(context,R.color.white)).cornerRadius(50).build()


            setContentView(view)

        }

    }

}