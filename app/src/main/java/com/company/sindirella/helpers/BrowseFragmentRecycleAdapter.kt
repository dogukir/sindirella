package com.company.sindirella.helpers

import android.content.Context
import android.media.Image
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.PagerAdapter
import com.company.sindirella.R
import java.util.ArrayList
import top.defaults.drawabletoolbox.DrawableBuilder

class BrowseFragmentRecycleAdapter(private val context: Context?) : RecyclerView.Adapter<BrowseFragmentRecycleAdapter.ViewHolder>() {



    private var clothesImage : ImageView? = null
    private var favorite : ImageView? = null
    private var titleText : TextView? = null
    private var explanationText : TextView? = null
    private var pricesText : TextView? = null
    private var examinedButton : Button? = null
    private var items: ArrayList<String> = ArrayList()
    private var mainLayout : ConstraintLayout? = null



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.browse_viewpager_item_view, parent, false)


        clothesImage = v.findViewById<ImageView>(R.id.clothesImage)
        favorite = v.findViewById<ImageView>(R.id.favorite)
        titleText = v.findViewById<TextView>(R.id.titleText)
        explanationText = v.findViewById<TextView>(R.id.explanationText)
        pricesText = v.findViewById<TextView>(R.id.pricesText)
        examinedButton = v.findViewById<Button>(R.id.examinedButton)
        mainLayout = v.findViewById(R.id.mainLayout)




        examinedButton?.background = DrawableBuilder().solidColor(ContextCompat.getColor(context!!,R.color.black)).cornerRadius(50).build()
        mainLayout?.background = DrawableBuilder().solidColor(ContextCompat.getColor(context!!,R.color.white)).cornerRadius(50).build()

        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        titleText?.text = items.get(position)

        holder.setIsRecyclable(false)
    }

    override fun getItemCount(): Int {

        return items.size

    }

    fun add(stringVal: String) {

        /*if (preferencesQuery.id == -1){

        }else{
            items.add(preferencesQuery)
            filteredList.add(preferencesQuery)
            notifyDataSetChanged()
        }*/

        items.add(stringVal)
        notifyDataSetChanged()

    }

    fun clearItems(){

        notifyDataSetChanged()

    }

    fun addAll(queries: ArrayList<String>) {

        if (!queries.isNullOrEmpty()) {

            for (query in queries) {

                add(query)

            }

        }

    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        //val button: CustomCheckBox = itemView.findViewById(R.id.radioButton)
        //val text:AppTextView = itemView.findViewById(R.id.textLabel)

    }


}