package com.company.sindirella.helpers

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.viewpager.widget.PagerAdapter
import com.company.sindirella.R
import java.util.ArrayList
import top.defaults.drawabletoolbox.DrawableBuilder

class CommentDetailViewPagerAdapter(private val context: Context,private val data : ArrayList<String>) : PagerAdapter() {

    private var viewTop : View? = null
    private var viewBottom : View? = null
    private var mainLayout : ConstraintLayout? = null


    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val v = LayoutInflater.from(context)
        val layout = v.inflate(R.layout.commet_detail_item_view,container,false)

        viewTop = layout.findViewById(R.id.viewTop)
        viewBottom = layout.findViewById(R.id.viewBottom)
        mainLayout = layout.findViewById(R.id.mainLayout)





        mainLayout?.background = DrawableBuilder().solidColor(ContextCompat.getColor(context,R.color.white)).build()
        viewTop?.background = DrawableBuilder().solidColor(ContextCompat.getColor(context,R.color.comment_detail_view_top)).cornerRadii(50,50,0,0).build()
        viewBottom?.background = DrawableBuilder().solidColor(ContextCompat.getColor(context,R.color.comment_detail_view_bottom)).cornerRadii(0,0,50,50).build()

        container.addView(layout)

        return layout
    }


    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }



    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object`
    }

    override fun getCount(): Int {
        return data.size
    }


}