package com.company.sindirella.helpers

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.company.sindirella.R
import top.defaults.drawabletoolbox.DrawableBuilder
import java.util.ArrayList

class BasketRecycleAdapter(private val context: Context?, private var data : ArrayList<String>): RecyclerView.Adapter<BasketRecycleAdapter.ViewHolder>() {


    private var imageClothes : ImageView? = null
    private var textBrand : TextView? = null
    private var textTitle : TextView? = null
    private var textCargo : TextView? = null
    private var textPrices : TextView? = null
    private var textTrash : ImageView? = null
    private var mainLayout : ConstraintLayout? = null


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.basket_items_view,parent,false)

        imageClothes = v.findViewById(R.id.imageClothes)
        textBrand = v.findViewById(R.id.textBrand)
        textTitle = v.findViewById(R.id.textTitle)
        textCargo = v.findViewById(R.id.textCargo)
        textPrices = v.findViewById(R.id.textPrices)
        textTrash = v.findViewById(R.id.textTrash)
        mainLayout = v.findViewById(R.id.mainLayout)




        mainLayout?.background = DrawableBuilder().solidColor(ContextCompat.getColor(context!!,R.color.white)).cornerRadius(50).build()

        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        textBrand?.text = data.get(position)



        holder.setIsRecyclable(false)
    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        //val button: CustomCheckBox = itemView.findViewById(R.id.radioButton)
        //val text:AppTextView = itemView.findViewById(R.id.textLabel)

    }

}