package com.company.sindirella.helpers

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.company.sindirella.R
import top.defaults.drawabletoolbox.DrawableBuilder
import java.util.ArrayList

class CommentActivityRecyclerAdapter(private val context: Context?, private var data : ArrayList<String>): RecyclerView.Adapter<CommentActivityRecyclerAdapter.ViewHolder>() {


    private var viewTop : View? = null
    private var viewBottom : View? = null
    private var buttonComment : Button? = null
    private var textViewName : TextView? = null


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.comment_recycler_item_view,parent,false)

        viewTop = v.findViewById(R.id.viewTop)
        viewBottom = v.findViewById(R.id.viewBottom)
        buttonComment = v.findViewById(R.id.buttonComment)
        textViewName = v.findViewById(R.id.textViewName)




        viewTop?.background = DrawableBuilder().solidColor(ContextCompat.getColor(context!!,R.color.view_background)).cornerRadii(50,50,0,0).build()
        viewBottom?.background = DrawableBuilder().solidColor(ContextCompat.getColor(context!!,R.color.white)).cornerRadii(0,0,50,50).build()
        buttonComment?.background = DrawableBuilder().solidColor(ContextCompat.getColor(context!!,R.color.transparent)).strokeColor(ContextCompat.getColor(context!!,R.color.black)).strokeWidth(3).cornerRadius(50).build()

        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        textViewName?.text = data.get(position)



        holder.setIsRecyclable(false)
    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        //val button: CustomCheckBox = itemView.findViewById(R.id.radioButton)
        //val text:AppTextView = itemView.findViewById(R.id.textLabel)

    }

}