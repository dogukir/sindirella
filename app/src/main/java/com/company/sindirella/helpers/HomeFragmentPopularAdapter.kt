package com.company.sindirella.helpers

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.company.sindirella.R
import top.defaults.drawabletoolbox.DrawableBuilder

class HomeFragmentPopularAdapter(private val context: Context?) : RecyclerView.Adapter<HomeFragmentPopularAdapter.ViewHolder>() {


    /*private var items: ArrayList<FilterUniStudentAllData> = ArrayList()
    private var filteredList: ArrayList<FilterUniStudentAllData> = ArrayList()
    var mSelectedItems: ArrayList<FilterUniStudentAllData> = ArrayList()
    var relativeLayout : RelativeLayout? = null
    var allButtons : ArrayList<CustomCheckBox> = ArrayList()
    var sendDataFilterUni : sendSelectFiltersUni? = null
    var checkBoxControll: ArrayList<Int?>? = null*/

    private var titlePopular : TextView? = null
    private var explanationPopular : TextView? = null
    private var pricesPopular : TextView? = null
    private var items: ArrayList<String> = ArrayList()
    private var mainLayout : ConstraintLayout? = null


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.mainpage_popular_view, parent, false)


        var titlePopular = v.findViewById<TextView>(R.id.titlePopular)
        var explanationPopular = v.findViewById<TextView>(R.id.explanationPopular)
        var pricesPopular = v.findViewById<TextView>(R.id.pricesPopular)
        mainLayout = v.findViewById(R.id.mainLayout)





        mainLayout?.background = DrawableBuilder().solidColor(ContextCompat.getColor(context!!,R.color.white)).cornerRadius(50).build()


        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        /*if (position == 0){
            asd?.setBackgroundColor(Color.parseColor(GlobalVars.Design.filterFirstColor))
        }*/

        /*val item = filteredList[position]
        var isControll = false

        for (i in 0..checkBoxControll?.size!!-1){
            if (checkBoxControll?.get(i) == item.id){
                isControll = true
            }
        }

        if (isControll == true){
            holder.button.isChecked = true
        }else if ( isControll == false){
            holder.button.isChecked = mSelectedItems.contains(item)
        }

        holder.text.text = item.name
        holder.setIsRecyclable(false)*/

        titlePopular?.text = items.get(position)

        holder.setIsRecyclable(false)
    }

    override fun getItemCount(): Int {

        return items.size

    }

    fun add(stringVal: String) {

        /*if (preferencesQuery.id == -1){

        }else{
            items.add(preferencesQuery)
            filteredList.add(preferencesQuery)
            notifyDataSetChanged()
        }*/

        items.add(stringVal)
        notifyDataSetChanged()

    }

    fun clearItems(){

        notifyDataSetChanged()

    }

    fun addAll(queries: ArrayList<String>) {

        if (!queries.isNullOrEmpty()) {

            for (query in queries) {

                add(query)

            }

        }

    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        //val button: CustomCheckBox = itemView.findViewById(R.id.radioButton)
        //val text:AppTextView = itemView.findViewById(R.id.textLabel)

    }

}
