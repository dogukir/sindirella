package com.company.sindirella.response.District

import com.google.gson.annotations.SerializedName

class DistrictResponse {

    @SerializedName("id")
    var id: Int? = null

    @SerializedName("name")
    var name: String? = null

    init {
        id = null
        name = null
    }

}