package com.company.sindirella.response.HelpPage

import com.google.gson.annotations.SerializedName

class HelpPageResponse {

    @SerializedName("id")
    var id: Int? = null

    @SerializedName("content")
    var content: String? = null

    @SerializedName("title")
    var title: String? = null

    init {
        id = null
        content = null
        title = null
    }

}