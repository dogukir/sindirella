package com.company.sindirella.response.Comment

import com.google.gson.annotations.SerializedName
import java.util.ArrayList

class GetCommentsResponse {

    @SerializedName("id")
    var id: Int? = null

    @SerializedName("title")
    var title: String? = null

    @SerializedName("body")
    var body: String? = null

    @SerializedName("hasTitle")
    var hasTitle: Boolean? = null

    @SerializedName("client")
    var client: ClientComments? = null

    @SerializedName("product")
    var product: String? = null

    @SerializedName("rating")
    var rating: Int? = null

    @SerializedName("ratingPerc")
    var ratingPerc: Int? = null

    @SerializedName("formattedDate")
    var formattedDate: String? = null

    @SerializedName("medias")
    var medias: ArrayList<Medias>? = null

    init {
        id = null
        title = null
        body = null
        hasTitle = null
        client = null
        product = null
        rating = null
        ratingPerc = null
        formattedDate = null
        medias = null
    }

}