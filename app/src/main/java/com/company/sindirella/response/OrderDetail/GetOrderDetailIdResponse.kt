package com.company.sindirella.response.OrderDetail

import com.google.gson.annotations.SerializedName

class GetOrderDetailIdResponse {

    @SerializedName("id")
    var id: Int? = null

    @SerializedName("status")
    var status: Int? = null

    @SerializedName("inventory")
    var inventory: String? = null

    @SerializedName("fromDate")
    var fromDate: String? = null

    @SerializedName("toDate")
    var toDate: String? = null

    @SerializedName("amountTotal")
    var amountTotal: Int? = null

    @SerializedName("theOrder")
    var theOrder: String? = null

    @SerializedName("basket")
    var basket: String? = null

    @SerializedName("client")
    var client: String? = null

    @SerializedName("days")
    var days: Int? = null

    @SerializedName("product")
    var product: String? = null

    @SerializedName("createdAt")
    var createdAt: String? = null

    @SerializedName("updatedAt")
    var updatedAt: String? = null

    @SerializedName("deletedAt")
    var deletedAt: String? = null

    @SerializedName("deleted")
    var deleted: Boolean? = null

    init {
        id = null
        status = null
        inventory = null
        fromDate = null
        toDate = null
        amountTotal = null
        theOrder = null
        basket = null
        client = null
        days = null
        product = null
        createdAt = null
        updatedAt = null
        deletedAt = null
        deleted = null
    }


}