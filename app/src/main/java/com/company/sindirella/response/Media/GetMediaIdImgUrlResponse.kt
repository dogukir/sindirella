package com.company.sindirella.response.Media

import com.google.gson.annotations.SerializedName

class GetMediaIdImgUrlResponse {

    @SerializedName("id")
    var id: Int? = null

    init {
        id = null
    }


}