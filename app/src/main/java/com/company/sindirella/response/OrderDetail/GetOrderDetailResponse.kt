package com.company.sindirella.response.OrderDetail

import com.google.gson.annotations.SerializedName

class GetOrderDetailResponse {

    @SerializedName("id")
    var id : Int? = null

    @SerializedName("amountTotal")
    var amountTotal : Int? = null

    @SerializedName("days")
    var days : Int? = null

    @SerializedName("product")
    var product : String? = null

    init {
        id = null
        amountTotal = null
        days = null
        product = null
    }


}