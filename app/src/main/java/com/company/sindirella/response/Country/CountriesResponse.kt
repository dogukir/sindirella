package com.company.sindirella.response.Country

import com.google.gson.annotations.SerializedName

class CountriesResponse {

    @SerializedName("id")
    var id: Int? = null

    @SerializedName("name")
    var name: String? = null

    @SerializedName("code")
    var code: String? = null

    init {
        id = null
        name = null
        code = null
    }

}