package com.company.sindirella.response.Invertory

import com.google.gson.annotations.SerializedName

class InventoriesResponse {

    @SerializedName("id")
    var id: Int? = null

    @SerializedName("product")
    var product: String? = null

    @SerializedName("size")
    var size: String? = null

    init {
        id = null
        product = null
        size = null
    }

}