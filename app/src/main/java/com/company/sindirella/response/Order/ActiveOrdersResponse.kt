package com.company.sindirella.response.Order

import com.google.gson.annotations.SerializedName
import java.util.ArrayList

class ActiveOrdersResponse {

    @SerializedName("id")
    var id : Int? = null

    @SerializedName("orderedAt")
    var orderedAt : String? = null

    @SerializedName("amountNet")
    var amountNet : Int? = null

    @SerializedName("amountVat")
    var amountVat : Int? = null

    @SerializedName("amountShipment")
    var amountShipment : Int? = null

    @SerializedName("amountTotal")
    var amountTotal : Int? = null

    @SerializedName("orderDetails")
    var orderDetails : ArrayList<OrderDetail>? = null

    @SerializedName("payment")
    var payment : Payment? = null

    @SerializedName("shipmentAddress")
    var shipmentAddress : ShipmentAddress? = null

    @SerializedName("invoiceAddress")
    var invoiceAddress : InvoiceAddress? = null

    @SerializedName("status")
    var status : Int? = null

    @SerializedName("statusStr")
    var statusStr : String? = null

    @SerializedName("transactionId")
    var transactionId : String? = null

    init {
        id = null
        orderedAt = null
        amountNet = null
        amountVat = null
        amountShipment = null
        amountTotal = null
        orderDetails = null
        payment = null
        shipmentAddress = null
        invoiceAddress = null
        status = null
        statusStr = null
        transactionId = null
    }


}

class OrderDetail {

    @SerializedName("id")
    var id : Int? = null

    @SerializedName("invertory")
    var invertory : String? = null

    @SerializedName("toDate")
    var toDate : String? = null

    @SerializedName("basket")
    var basket : String? = null

    @SerializedName("days")
    var days : Int? = null

    @SerializedName("product")
    var product : String? = null

    init {
        id = null
        invertory = null
        toDate = null
        basket = null
        days = null
        product = null
    }

}

class Payment {

    @SerializedName("typeStr")
    var typeStr : String? = null

    init {
        typeStr = null
    }

}

class ShipmentAddress {

    @SerializedName("name")
    var name : String? = null

    @SerializedName("address")
    var address : String? = null

    @SerializedName("country")
    var country : String? = null

    @SerializedName("county")
    var county : String? = null

    @SerializedName("city")
    var city : String? = null

    @SerializedName("zipCode")
    var zipCode : String? = null

    @SerializedName("district")
    var district : String? = null

    @SerializedName("receiverName")
    var receiverName : String? = null

    @SerializedName("phone")
    var phone : String? = null

    init {
        name = null
        address = null
        country = null
        county = null
        city = null
        zipCode = null
        district = null
        receiverName = null
        phone = null
    }

}

class InvoiceAddress {

    @SerializedName("name")
    var name : String? = null

    @SerializedName("address")
    var address : String? = null

    @SerializedName("country")
    var country : String? = null

    @SerializedName("county")
    var county : String? = null

    @SerializedName("city")
    var city : String? = null

    @SerializedName("zipCode")
    var zipCode : String? = null

    @SerializedName("district")
    var district : String? = null

    @SerializedName("receiverName")
    var receiverName : String? = null

    @SerializedName("phone")
    var phone : String? = null

    init {
        name = null
        address = null
        country = null
        county = null
        city = null
        zipCode = null
        district = null
        receiverName = null
        phone = null
    }

}