package com.company.sindirella.response.BasketItem

import com.google.gson.annotations.SerializedName

class BasketItemsIdResponse {

    @SerializedName("id")
    var id : Int? = null

    @SerializedName("client")
    var client : String? = null

    @SerializedName("product")
    var product : String? = null

    @SerializedName("saleType")
    var saleType : Int? = null

    @SerializedName("quantity")
    var quantity : Int? = null

    @SerializedName("formDate")
    var formDate : String? = null

    @SerializedName("toDate")
    var toDate : String? = null

    @SerializedName("status")
    var status : Int? = null

    @SerializedName("orderDetail")
    var orderDetail : String? = null

    @SerializedName("deletedAt")
    var deletedAt : String? = null

    @SerializedName("deleted")
    var deleted : Boolean? = null

    @SerializedName("createdAt")
    var createdAt : String? = null

    @SerializedName("updatedAt")
    var updatedAt : String? = null

    @SerializedName("createdBy")
    var createdBy : String? = null

    @SerializedName("updatedBy")
    var updatedBy : String? = null


    init {
        id = null
        client = null
        product = null
        saleType = null
        quantity = null
        formDate = null
        toDate = null
        status = null
        orderDetail = null
        deletedAt = null
        deleted = null
        createdAt = null
        updatedAt = null
        createdBy = null
        updatedBy = null
    }


}