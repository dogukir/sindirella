package com.company.sindirella.response.Product

import com.google.gson.annotations.SerializedName
import java.util.ArrayList

class ProductResponse {

    @SerializedName("id")
    var id: Int? = null

    @SerializedName("name")
    var name: String? = null

    @SerializedName("description")
    var description: String? = null

    @SerializedName("notes")
    var notes: String? = null

    @SerializedName("brand")
    var brand: String? = null

    @SerializedName("color")
    var color: String? = null

    @SerializedName("isRental")
    var isRental: Boolean? = null

    @SerializedName("priceRental")
    var priceRental: Int? = null

    @SerializedName("priceSale")
    var priceSale: Int? = null

    @SerializedName("vatRate")
    var vatRate: Int? = null

    @SerializedName("tags")
    var tags: ArrayList<String>? = null

    @SerializedName("pictures")
    var pictures: ArrayList<String>? = null

    @SerializedName("url")
    var url: String? = null

    @SerializedName("fullLink")
    var fullLink: String? = null

    init {
        id = null
        name = null
        description = null
        notes = null
        brand = null
        color = null
        isRental = null
        priceRental = null
        priceSale = null
        vatRate = null
        tags = null
        pictures = null
        url = null
        fullLink = null
    }



}