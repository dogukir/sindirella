package com.company.sindirella.response.City

import com.google.gson.annotations.SerializedName

class CitiesResponse {

    @SerializedName("id")
    var id : Int? = null

    @SerializedName("name")
    var name : String? = null

    init {
        id = null
        name = null
    }

}