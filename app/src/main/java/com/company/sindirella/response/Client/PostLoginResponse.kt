package com.company.sindirella.response.Client

import com.google.gson.annotations.SerializedName
import java.util.ArrayList

class PostLoginResponse {

    @SerializedName("id")
    var id : Int? = null

    @SerializedName("retypedPassword")
    var retypedPassword : String? = null

    @SerializedName("email")
    var email : String? = null

    @SerializedName("phone")
    var phone : String? = null

    @SerializedName("password")
    var password : String? = null

    @SerializedName("socialFbId")
    var socialFbId : String? = null

    @SerializedName("socialGoogleId")
    var socialGoogleId : String? = null

    @SerializedName("name")
    var name : String? = null

    @SerializedName("birthday")
    var birthday : String? = null

    @SerializedName("lastLogin")
    var lastLogin : String? = null

    @SerializedName("extra")
    var extra : ArrayList<String>? = null

    @SerializedName("clientAddresses")
    var clientAddresses : ArrayList<String>? = null

    @SerializedName("orders")
    var orders : ArrayList<String>? = null

    @SerializedName("basketItems")
    var basketItems : ArrayList<String>? = null

    @SerializedName("payments")
    var payments : ArrayList<String>? = null

    @SerializedName("invoices")
    var invoices : ArrayList<String>? = null

    @SerializedName("roles")
    var roles : ArrayList<String>? = null

    @SerializedName("salt")
    var salt : String? = null

    @SerializedName("username")
    var username : String? = null

    @SerializedName("favProducts")
    var favProducts : ArrayList<String>? = null

    @SerializedName("city")
    var city : String? = null

    @SerializedName("size")
    var size : String? = null

    @SerializedName("country")
    var country : String? = null

    @SerializedName("comments")
    var comments : ArrayList<String>? = null

    @SerializedName("tokenId")
    var tokenId : String? = null

    @SerializedName("deletedAt")
    var deletedAt : String? = null

    @SerializedName("deleted")
    var deleted : String? = null

    @SerializedName("createdAt")
    var createdAt : String? = null

    @SerializedName("updatedAt")
    var updatedAt : String? = null

    init {
        id = null
        retypedPassword = null
        email = null
        phone = null
        password = null
        socialFbId = null
        socialGoogleId = null
        name = null
        birthday = null
        lastLogin = null
        extra = null
        clientAddresses = null
        orders = null
        basketItems = null
        payments = null
        invoices = null
        roles = null
        salt = null
        username = null
        favProducts = null
        city = null
        size = null
        country = null
        comments = null
        tokenId = null
        deletedAt = null
        deleted = null
        createdAt = null
        updatedAt = null
    }



}