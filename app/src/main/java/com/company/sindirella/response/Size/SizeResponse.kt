package com.company.sindirella.response.Size

import com.google.gson.annotations.SerializedName

class SizeResponse {

    @SerializedName("id")
    var id : Int? = null

    @SerializedName("name")
    var name : String? = null

    init {
        id = null
        name = null
    }

}