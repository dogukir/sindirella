package com.company.sindirella.response.Invoice

import com.google.gson.annotations.SerializedName
import java.util.ArrayList

class InvoiceResponse {

    @SerializedName("id")
    var id : Int? = null

    @SerializedName("payment")
    var payment : String? = null

    @SerializedName("shippedAt")
    var shippedAt : String? = null

    @SerializedName("amountNet")
    var amountNet : Int? = null

    @SerializedName("amountVat")
    var amountVat : Int? = null

    @SerializedName("amountTotal")
    var amountTotal : Int? = null

    @SerializedName("client")
    var client : String? = null

    @SerializedName("invoiceLines")
    var invoiceLines : ArrayList<String>? = null

    @SerializedName("deletedAt")
    var deletedAt : String? = null

    @SerializedName("deleted")
    var deleted : Boolean? = null

    @SerializedName("createdAt")
    var createdAt : String? = null

    @SerializedName("updatedAt")
    var updatedAt : String? = null

    init {
        id = null
        payment = null
        shippedAt = null
        amountNet = null
        amountVat = null
        amountTotal = null
        client = null
        invoiceLines = null
        deletedAt = null
        deleted = null
        createdAt = null
        updatedAt = null
    }



}