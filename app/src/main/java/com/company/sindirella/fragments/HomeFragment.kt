package com.company.sindirella.fragments

import android.annotation.TargetApi
import android.app.DatePickerDialog
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import com.company.sindirella.R
import com.company.sindirella.helpers.HomeFragmentPopularAdapter
import com.company.sindirella.helpers.HomeFragmentViewPagerAdapter
import com.company.sindirella.helpers.HomeFragmentWhichActivityAdapter
import com.company.sindirella.helpers.HowWorkingViewPagerAdapter
import top.defaults.drawabletoolbox.DrawableBuilder
import java.text.SimpleDateFormat
import java.util.*

class HomeFragment : Fragment() {

    private var mainLayout: ConstraintLayout? = null
    private var spinnerAct: Spinner? = null
    private var viewPagerHome : ViewPager? = null
    private var viewPagerAdapter: HomeFragmentViewPagerAdapter? = null
    private var startCalendar : TextView? = null
    private var endCalendar : TextView? = null
    private var recycleWhichActivity: RecyclerView? = null
    private var whichActivityAdapter: HomeFragmentWhichActivityAdapter? = null
    private var recyclePopular: RecyclerView? = null
    private var homeFragmentPopularAdapter: HomeFragmentPopularAdapter? = null
    private var viewSpinner : View? = null
    private var viewStartDate : View? = null
    private var viewEndDate : View? = null
    private var showButton : Button? = null


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val inflater = LayoutInflater.from(context)
        val view = inflater.inflate(R.layout.fragment_home, container, false)


        mainLayout = view.findViewById(R.id.mainLayout)
        spinnerAct = view.findViewById(R.id.spinnerAct)
        viewPagerHome = view.findViewById(R.id.viewPagerHome)
        startCalendar = view.findViewById(R.id.startCalendar)
        endCalendar = view.findViewById(R.id.endCalendar)
        recycleWhichActivity = view.findViewById(R.id.recycleWhichActivity)
        recyclePopular = view.findViewById(R.id.recyclePopular)
        viewSpinner = view.findViewById(R.id.viewSpinner)
        viewStartDate = view.findViewById(R.id.viewStartDate)
        viewEndDate = view.findViewById(R.id.viewEndDate)
        showButton = view.findViewById(R.id.showButton)


        var arr = resources.getStringArray(R.array.spinner_default_value)

        var adapter = ArrayAdapter<String>(context!!, R.layout.spinner_item, R.id.spinnerTextView, arr)

        spinnerAct?.adapter = adapter

        var arrViewPager = ArrayList<String>()

        arrViewPager.add("doğu")
        arrViewPager.add("kan")
        arrViewPager.add("kır")
        arrViewPager.add("doğukan")
        arrViewPager.add("kankır")

        viewPagerAdapter = HomeFragmentViewPagerAdapter(context!!,arrViewPager)

        viewPagerHome?.adapter = viewPagerAdapter

        viewPagerHome?.pageMargin = 30
        viewPagerHome?.setPadding(50,0,50,0)
        viewPagerHome?.clipToPadding = false
        viewPagerHome?.offscreenPageLimit = 0

        whichActivityAdapter = HomeFragmentWhichActivityAdapter(context)
        recycleWhichActivity?.adapter = whichActivityAdapter


        homeFragmentPopularAdapter = HomeFragmentPopularAdapter(context)
        recyclePopular?.adapter = homeFragmentPopularAdapter



        val layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)  //fragment içinte tanımlanan

        recyclePopular?.layoutManager = layoutManager
        recyclePopular?.itemAnimator = DefaultItemAnimator()


        homeFragmentPopularAdapter?.addAll(arrViewPager)  //data yollandı

        whichActivityAdapter?.addAll(arrViewPager)


        recyclePopular?.layoutManager = LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false)
        recycleWhichActivity?.layoutManager = LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false)



        var controllVal = 0

        val sdf = SimpleDateFormat("dd/MM/yyyy", Locale.ROOT)
        var calendarStart = Calendar.getInstance()
        var calendarEnd = Calendar.getInstance()

        val date = DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
            if (controllVal == 1) {

                calendarStart.set(Calendar.YEAR, year)
                calendarStart.set(Calendar.MONTH, monthOfYear)
                calendarStart.set(Calendar.DAY_OF_MONTH, dayOfMonth)

                startCalendar?.text = sdf.format(calendarStart.time)
                startCalendar?.setTextColor(resources.getColor(R.color.black))

            } else {

                calendarEnd.set(Calendar.YEAR, year)
                calendarEnd.set(Calendar.MONTH, monthOfYear)
                calendarEnd.set(Calendar.DAY_OF_MONTH, dayOfMonth)

                endCalendar?.text = sdf.format(calendarEnd.time)

            }
        }

        startCalendar?.setOnClickListener {
            controllVal = 1
            DatePickerDialog(context!!, R.style.DatePickerTheme, date, calendarStart.get(Calendar.YEAR), calendarStart.get(Calendar.MONTH), calendarStart.get(Calendar.DAY_OF_MONTH)).show()
        }

        endCalendar?.setOnClickListener {
            controllVal = 2
            DatePickerDialog(context!!, R.style.DatePickerTheme, date, calendarEnd.get(Calendar.YEAR), calendarEnd.get(Calendar.MONTH), calendarEnd.get(Calendar.DAY_OF_MONTH)).show()
        }






        setUI()








        return view
    }


    fun setUI() {

        showButton?.background = DrawableBuilder().solidColor(ContextCompat.getColor(context!!,R.color.show_button)).cornerRadius(50).build()
        viewSpinner?.background = DrawableBuilder().solidColor(ContextCompat.getColor(context!!,R.color.view_background)).cornerRadius(50).build()
        viewStartDate?.background = DrawableBuilder().solidColor(ContextCompat.getColor(context!!,R.color.view_background)).cornerRadius(50).build()
        viewEndDate?.background = DrawableBuilder().solidColor(ContextCompat.getColor(context!!,R.color.view_background)).cornerRadius(50).build()
        mainLayout?.setBackgroundColor(resources.getColor(R.color.how_working_status_bar))
        //viewPagerHome?.background = DrawableBuilder().cornerRadius(50).build()
        //window.statusBarColor = resources.getColor(R.color.how_working_status_bar)
        //window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR

    }
}