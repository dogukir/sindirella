package com.company.sindirella.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.fragment.app.Fragment
import com.company.sindirella.R
import com.company.sindirella.activitys.*
import com.company.sindirella.view.MenuItemView
import java.util.ArrayList

class MenuFragment : Fragment(){

    private var itemLayout : LinearLayout? = null


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        var view = inflater.inflate(R.layout.fragment_menu,container,false)

        itemLayout = view.findViewById(R.id.itemLayout)

        var menuArray = ArrayList<String>()

        menuArray.add("Yeni Gelenler")
        menuArray.add("Sonbahar")
        menuArray.add("İletişim")
        menuArray.add("Bizi Tanıyın")
        menuArray.add("Kullanım Şartları")
        menuArray.add("Gizlilik & Güvenlik")
        menuArray.add("Nasıl Kiralarım")
        menuArray.add("Randevu")
        menuArray.add("Teslimat & İade")
        menuArray.add("Satış Sözleşmesi")
        menuArray.add("Facebook")
        menuArray.add("Instagram")

        for (i in 0..menuArray.size-1){

            val menuView = MenuItemView(context)

            menuView.setTitle(menuArray.get(i))

            menuView.setOnClickListener {

                if (menuArray.get(i).equals("Yeni Gelenler")){
                    val intent = Intent(context, LoginActivity::class.java)
                    startActivity(intent)
                }else if (menuArray.get(i).equals("Sonbahar")){
                    val intent = Intent(context, OrderSummary::class.java)
                    startActivity(intent)
                }else if (menuArray.get(i).equals("İletişim")){
                    val intent = Intent(context, MyAccountActivity::class.java)
                    startActivity(intent)
                }else if (menuArray.get(i).equals("Bizi Tanıyın")){
                    val intent = Intent(context, CommentsActivity::class.java)
                    startActivity(intent)
                }else if (menuArray.get(i).equals("Kullanım Şartları")){
                    val intent = Intent(context, CommentDetail::class.java)
                    startActivity(intent)
                }



            }

            itemLayout?.addView(menuView)

        }



        
        return view
    }

}