package com.company.sindirella.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import com.company.sindirella.R
import com.company.sindirella.activitys.FilterActivity
import com.company.sindirella.helpers.BrowseFragmentRecycleAdapter
import com.company.sindirella.helpers.HomeFragmentPopularAdapter
import com.company.sindirella.view.SortPopUp
import top.defaults.drawabletoolbox.DrawableBuilder
import java.util.ArrayList

class BrowseFragment : Fragment() {


    private var titleText : TextView? = null
    private var totalProduct : TextView? = null
    private var filterText : TextView? = null
    private var sortText : TextView? = null
    private var recycler : RecyclerView? = null
    private var adapter : BrowseFragmentRecycleAdapter? = null
    private var viewFilter : View? = null
    private var viewSort : View? = null
    private var menuIcon : ImageView? = null
    private var displayChangeImage : ImageView? = null



    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        var view = inflater.inflate(R.layout.fragment_browse,container,false)


        recycler = view.findViewById(R.id.recycler)
        viewFilter = view.findViewById(R.id.viewFilter)
        viewSort = view.findViewById(R.id.viewSort)
        menuIcon = view.findViewById(R.id.menuIcon)
        sortText = view.findViewById(R.id.sortText)
        filterText = view.findViewById(R.id.filterText)
        displayChangeImage = view.findViewById(R.id.displayChangeImage)

        var arr = ArrayList<String>()

        arr.add("dogukan")
        arr.add("dogukan")
        arr.add("dogukan")
        arr.add("dogukan")
        arr.add("dogukan")
        arr.add("dogukan")


        adapter = BrowseFragmentRecycleAdapter(context)
        recycler?.adapter = adapter



        val layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)  //fragment içinte tanımlanan

        recycler?.layoutManager = layoutManager
        recycler?.itemAnimator = DefaultItemAnimator()


        adapter?.addAll(arr)  //data yollandı

        var value = 0


        displayChangeImage?.setOnClickListener {

            if (value == 0){
                adapter = BrowseFragmentRecycleAdapter(context)
                recycler?.adapter = adapter

                val layoutManager2 = GridLayoutManager(context,2)

                recycler?.layoutManager = layoutManager2
                recycler?.itemAnimator = DefaultItemAnimator()

                adapter?.addAll(arr)
                value = 1
            }else{
                adapter = BrowseFragmentRecycleAdapter(context)
                recycler?.adapter = adapter



                val layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)  //fragment içinte tanımlanan

                recycler?.layoutManager = layoutManager
                recycler?.itemAnimator = DefaultItemAnimator()


                adapter?.addAll(arr)
                value = 0
            }


        }


        sortText?.setOnClickListener {

            var arr = ArrayList<String>()

            var dialogView = SortPopUp(context!!,arr)
            dialogView.createView(R.layout.sort_pop_up)
            dialogView.show()
            dialogView.setCanceledOnTouchOutside(false)


            dialogView.exitButton?.setOnClickListener {

                dialogView.dismiss()

            }

        }


        filterText?.setOnClickListener {

            val intent = Intent(context,FilterActivity::class.java)
            startActivity(intent)

        }



        setUI()


        return view
    }


    fun setUI(){

        viewFilter?.background = DrawableBuilder().strokeColor(resources.getColor(R.color.black)).strokeWidth(5).cornerRadius(50).build()
        viewSort?.background = DrawableBuilder().strokeColor(resources.getColor(R.color.black)).strokeWidth(5).cornerRadius(50).build()


    }

}