package com.company.sindirella.controller

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.NetworkResponseListener
import com.company.sindirella.requestApi.RequestRegister
import com.company.sindirella.response.Client.PostRegisterResponse
import com.google.gson.JsonObject

class LoginController(private var activity: AppCompatActivity?,private var fragment: Fragment?) {

    var postRegisterListener: onPostRegisterListener? = null

    fun postRegister(body: JsonObject?){

        RequestRegister(
            activity,
            fragment,
            body,
            object : NetworkResponseListener<PostRegisterResponse> {
                override fun onResponseReceived(response: PostRegisterResponse) {
                    postRegisterListener?.onPostRegisterListener(true, response, -1, "")
                }

                override fun onEmptyResponse(response: PostRegisterResponse?) {
                    postRegisterListener?.onPostRegisterListener(true, null, -1, "")
                }

                override fun onError(failMessage: Int, error: String) {
                    postRegisterListener?.onPostRegisterListener(false, null, failMessage, error)
                }

            })

    }

    interface onPostRegisterListener {
        fun onPostRegisterListener(responseOk:Boolean,register: PostRegisterResponse?, failMessage: Int, error: String)
    }


}