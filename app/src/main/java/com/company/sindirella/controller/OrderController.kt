package com.company.sindirella.controller

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.NetworkResponse
import com.company.sindirella.network.NetworkResponseListener
import com.company.sindirella.requestApi.*
import com.company.sindirella.response.Order.*
import com.google.gson.JsonObject
import java.util.ArrayList

class OrderController(private var activity: AppCompatActivity?, private var fragment: Fragment?) {

    fun getActiveOrders(id: Int?, idArray: ArrayList<Int>?, status: Int?, statusArray: ArrayList<Int>?, orderId : String?, page : Int?){

        RequestActiveOrders(activity,fragment,id,idArray,status,statusArray,orderId,page,object : NetworkResponseListener<ArrayList<ActiveOrdersResponse>> {
            override fun onResponseReceived(response: ArrayList<ActiveOrdersResponse>) {

            }

            override fun onEmptyResponse(response: ArrayList<ActiveOrdersResponse>?) {

            }

            override fun onError(failMessage: Int, error: String) {

            }

        })
    }


    fun getOrderHistory(id: Int?, idArray: ArrayList<Int>?, status: Int?, statusArray: ArrayList<Int>?, orderId : String?, page : Int?){

        RequestOrderHistory(activity,fragment,id,idArray,status,statusArray,orderId,page,object : NetworkResponseListener<ArrayList<OrderHistoryResponse>> {
            override fun onResponseReceived(response: ArrayList<OrderHistoryResponse>) {

            }

            override fun onEmptyResponse(response: ArrayList<OrderHistoryResponse>?) {

            }

            override fun onError(failMessage: Int, error: String) {

            }

        })
    }

    fun postOrders(body: JsonObject?){
        RequestOrders(activity,fragment,body,object : NetworkResponseListener<OrdersResponse> {
            override fun onResponseReceived(response: OrdersResponse) {

            }

            override fun onEmptyResponse(response: OrdersResponse?) {

            }

            override fun onError(failMessage: Int, error: String) {

            }

        })
    }

    fun GetOrdersId(id: String?){
        RequestGetOrdersId(activity,fragment,id,object : NetworkResponseListener<GetOrdersIdResponse> {
            override fun onResponseReceived(response: GetOrdersIdResponse) {

            }

            override fun onEmptyResponse(response: GetOrdersIdResponse?) {

            }

            override fun onError(failMessage: Int, error: String) {

            }

        })
    }

    fun PutOrdersId(id: String?,body: JsonObject?){
        RequestPutOrdersId(activity,fragment,id,body,object : NetworkResponseListener<PutOrdersIdResponse> {
            override fun onResponseReceived(response: PutOrdersIdResponse) {

            }

            override fun onEmptyResponse(response: PutOrdersIdResponse?) {

            }

            override fun onError(failMessage: Int, error: String) {

            }

        })
    }

    fun DeleteOrdersId(id: String?){
        RequestDeleteOrdersId(activity,fragment,id,object : NetworkResponseListener<PutOrdersIdResponse> {
            override fun onResponseReceived(response: PutOrdersIdResponse) {

            }

            override fun onEmptyResponse(response: PutOrdersIdResponse?) {

            }

            override fun onError(failMessage: Int, error: String) {

            }

        })
    }

}